/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QUERY_BUILDER_H_
#define QUERY_BUILDER_H_

#include "diagramitem.h"

#include <QMainWindow>

class DiagramScene;

QT_BEGIN_NAMESPACE
class QAction;
class QCheckBox;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QButtonGroup;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
class QGraphicsView;
class QWidgetAction;
QT_END_NAMESPACE

class QueryBuilder : public QMainWindow {
  Q_OBJECT

public:
  QueryBuilder();
  ~QueryBuilder();
  QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  QSize sizeHint() const Q_DECL_OVERRIDE;

  public slots:
  
  private slots:
  void buttonGroupClicked(int id);
  void deleteItem();
  void pointerGroupClicked(int id);
  void bringToFront();
  void sendToBack();
  void itemInserted(DiagramItem *item);
  void currentFontChanged(const QFont &font);
  void fontSizeChanged(const QString &size);
  void sceneScaleChanged(const QString &scale);
  void textColorChanged();
  void itemColorChanged();
  void lineColorChanged();
  void textButtonTriggered();
  void fillButtonTriggered();
  void lineButtonTriggered();
  void handleFontChange();
  void itemSelected(QGraphicsItem *item);
  void about();

private:

  void createButtons();
  void createActions();
  void createMenus();
  void createToolbars();
  QWidget *createBackgroundCellWidget(const QString &text,
                                      const QString &image);
  QWidget *createCellWidget(const QString &text,
                            DiagramItem::DiagramType type);
  QWidget *createOperatorsWidget(const QString &text);
  QMenu *createColorMenu(const char *slot, QColor defaultColor);
  QIcon createColorToolButtonIcon(const QString &image, QColor color);
  QIcon createColorIcon(QColor color);

  DiagramScene *scene;
  QGraphicsView *view;

  QAction *exitAction;
  QAction *addAction;
  QAction *deleteAction;

  QAction *toFrontAction;
  QAction *sendBackAction;
  QAction *aboutAction;

  QMenu *fileMenu;
  QMenu *itemMenu;
  QMenu *aboutMenu;

  QToolBar *textToolBar;
  QToolBar *editToolBar;
  QToolBar *colorToolBar;
  QToolBar *pointerToolbar;
  QToolBar *statusToolBar;

  QStatusBar *status_bar;

  QComboBox *sceneScaleCombo;
  QComboBox *itemColorCombo;
  QComboBox *textColorCombo;
  QComboBox *fontSizeCombo;
  QFontComboBox *fontCombo;

  QWidget *itemWidget;
  QButtonGroup *buttonGroup;
  QButtonGroup *pointerTypeGroup;
  QToolButton *fontColorToolButton;
  QToolButton *fillColorToolButton;
  QToolButton *lineColorToolButton;
  QAction *boldAction;
  QAction *underlineAction;
  QAction *italicAction;
  QAction *textAction;
  QAction *fillAction;
  QAction *lineAction;

  QToolButton *operator_selector_button_;
  QLabel *operator_current_selection_label_;
};

#endif // QUERY_BUILDER_H_
