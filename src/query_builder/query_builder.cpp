/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "arrow.h"
#include "diagramitem.h"
#include "diagramscene.h"
#include "diagramtextitem.h"
#include "query_builder.h"

#include <QtWidgets>

QueryBuilder::QueryBuilder() {

  createActions();
  createButtons();
  createMenus();

  scene = new DiagramScene();
  scene->setSceneRect(QRectF(0, 0, 4000, 4000));
  connect(scene, SIGNAL(itemInserted(DiagramItem*)),
          this, SLOT(itemInserted(DiagramItem*)));
  connect(scene, SIGNAL(itemSelected(QGraphicsItem*)),
          this, SLOT(itemSelected(QGraphicsItem*)));
  createToolbars();

  QHBoxLayout *layout = new QHBoxLayout;
  QVBoxLayout *vlayout = new QVBoxLayout;
  itemWidget->setContentsMargins(-1, -1, -1, -1);
  itemWidget->setMinimumHeight(50);
  itemWidget->resize(width(), 100);
  vlayout->setSpacing(0);
  vlayout->setMargin(0);
//  vlayout->addWidget(feature_space_selector_);
  vlayout->addWidget(itemWidget);
  layout->addLayout(vlayout);
  view = new QGraphicsView(scene);
  layout->addWidget(view);
  view->setAcceptDrops(true);
  layout->setSpacing(0);
  layout->setMargin(0);

  QWidget *widget = new QWidget;
  widget->setLayout(layout);
  
  setCentralWidget(widget);
  setWindowTitle(tr("Objects Explorer"));
  setObjectName(QString("DiagramExplorer"));
  setUnifiedTitleAndToolBarOnMac(true);
}

QueryBuilder::~QueryBuilder() {
  
}

QSize QueryBuilder::minimumSizeHint() const {
  return QSize(64 * 1, 64 * 1);
}

QSize QueryBuilder::sizeHint() const {
  return QSize(64 * 7, 64 * 7);
}

void QueryBuilder::buttonGroupClicked(int id) {
  if (id == 0) {
    //SetDiagramType(DiagramItem::DiagramType(id));
  }
}

void QueryBuilder::deleteItem() {
  foreach(QGraphicsItem *item, scene->selectedItems()) {
    if (item->type() == Arrow::Type) {
      scene->removeItem(item);
      Arrow *arrow = qgraphicsitem_cast<Arrow *>(item);
      arrow->startItem()->removeArrow(arrow);
      arrow->endItem()->removeArrow(arrow);
      delete item;
    }
  }

  foreach(QGraphicsItem *item, scene->selectedItems()) {
    if (item->type() == DiagramItem::Type)
      qgraphicsitem_cast<DiagramItem *>(item)->removeArrows();
    scene->removeItem(item);
    delete item;
  }
}

void QueryBuilder::pointerGroupClicked(int) {
  scene->setMode(DiagramScene::Mode(pointerTypeGroup->checkedId()));
}

void QueryBuilder::bringToFront() {
  if (scene->selectedItems().isEmpty())
    return;

  QGraphicsItem *selectedItem = scene->selectedItems().first();
  QList<QGraphicsItem *> overlapItems = selectedItem->collidingItems();

  qreal zValue = 0;
  foreach(QGraphicsItem *item, overlapItems) {
    if (item->zValue() >= zValue && item->type() == DiagramItem::Type)
      zValue = item->zValue() + 0.1;
  }
  selectedItem->setZValue(zValue);
}

void QueryBuilder::sendToBack() {
  if (scene->selectedItems().isEmpty())
    return;

  QGraphicsItem *selectedItem = scene->selectedItems().first();
  QList<QGraphicsItem *> overlapItems = selectedItem->collidingItems();

  qreal zValue = 0;
  foreach(QGraphicsItem *item, overlapItems) {
    if (item->zValue() <= zValue && item->type() == DiagramItem::Type)
      zValue = item->zValue() - 0.1;
  }
  selectedItem->setZValue(zValue);
}

void QueryBuilder::itemInserted(DiagramItem *item) {
  pointerTypeGroup->button(int(DiagramScene::MoveItem))->setChecked(true);
  scene->setMode(DiagramScene::Mode(pointerTypeGroup->checkedId())); 
  
  // Uncheck buttons
//  UncheckObjectsButtons();

  /*
  if (item->diagramType() == DiagramItem::PatchSet) {
    if (item->patchset_object_index() == -1 &&
        !AreStringsTheSame(item->get_name(), std::string("empty"))) {
      auto pso = objects_explorer_->OpenPatchSet(item->get_name());
      if (!pso->is_loaded()) {
        IVDA::Timer load_timer; load_timer.Start();
        pso->Load(objects_explorer_->root_data_dir() + std::string("/") +
                  item->get_name() + std::string(".pso"));
        item->SetFeatureDesc(pso->cur_feature());
        printf("Loading %s from patch set library took %f seconds.\n",
               item->get_name().c_str(), load_timer.Elapsed() / 1000.0f);
      }
      item->SetPatchSetObjectIndex(pso->source_desc().id);
    }
  }
  */
}

void QueryBuilder::currentFontChanged(const QFont &) {
  handleFontChange();
}

void QueryBuilder::fontSizeChanged(const QString &) {
  handleFontChange();
}

void QueryBuilder::sceneScaleChanged(const QString &scale) {
  double newScale = scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
  QMatrix oldMatrix = view->matrix();
  view->resetMatrix();
  view->translate(oldMatrix.dx(), oldMatrix.dy());
  view->scale(newScale, newScale);
}

void QueryBuilder::textColorChanged() {
  textAction = qobject_cast<QAction *>(sender());
  fontColorToolButton->setIcon(createColorToolButtonIcon(
    "resources/images/textpointer.png",
    qvariant_cast<QColor>(textAction->data())));
  textButtonTriggered();
}

void QueryBuilder::itemColorChanged() {
  fillAction = qobject_cast<QAction *>(sender());
  fillColorToolButton->setIcon(createColorToolButtonIcon(
    "resources/images/floodfill.png",
    qvariant_cast<QColor>(fillAction->data())));
  fillButtonTriggered();
}

void QueryBuilder::lineColorChanged() {
  lineAction = qobject_cast<QAction *>(sender());
  lineColorToolButton->setIcon(createColorToolButtonIcon(
    "resources/images/linecolor.png",
    qvariant_cast<QColor>(lineAction->data())));
  lineButtonTriggered();
}

void QueryBuilder::textButtonTriggered() {
  scene->setTextColor(qvariant_cast<QColor>(textAction->data()));
}

void QueryBuilder::fillButtonTriggered() {
  scene->setItemColor(qvariant_cast<QColor>(fillAction->data()));
}

void QueryBuilder::lineButtonTriggered() {
  scene->setLineColor(qvariant_cast<QColor>(lineAction->data()));
}

void QueryBuilder::handleFontChange() {
  QFont font = fontCombo->currentFont();
  font.setPointSize(fontSizeCombo->currentText().toInt());
  font.setWeight(boldAction->isChecked() ? QFont::Bold : QFont::Normal);
  font.setItalic(italicAction->isChecked());
  font.setUnderline(underlineAction->isChecked());

  scene->setFont(font);
}

void QueryBuilder::itemSelected(QGraphicsItem *item) {
  DiagramTextItem *textItem =
    qgraphicsitem_cast<DiagramTextItem *>(item);

  QFont font = textItem->font();
  fontCombo->setCurrentFont(font);
  fontSizeCombo->setEditText(QString().setNum(font.pointSize()));
  boldAction->setChecked(font.weight() == QFont::Bold);
  italicAction->setChecked(font.italic());
  underlineAction->setChecked(font.underline());
}

void QueryBuilder::about() {

  QMessageBox::about(
    this, tr("Keyboard + Mouse Commands"),
    tr("Right-click on bin: shows selected bin's patch pointers on image explorer.\n") + 
    tr("ALT + Right-click on bin: shows all bins' patch pointers on image explorer.\n") + 
    tr("SHIFT + Right-click on any bin: clears patch pointers on image explorer.\n") + 
    tr("CTRL + Right-click on any bin: deletes bin.\n") + 
    tr("SHIFT + Left-click on any patch: zooms to patch on image explorer.\n") + 
    tr("ALT + Left-click on any patch: resets image explorer's viewport settings.\n") + 
    tr("SHIFT + Left-click then drag to lower right side: select region of interest.\n"));
}

void QueryBuilder::createButtons() {
  buttonGroup = new QButtonGroup(this);
  buttonGroup->setExclusive(true);
  connect(buttonGroup, SIGNAL(buttonClicked(int)),
          this, SLOT(buttonGroupClicked(int)));
  QGridLayout *layout = new QGridLayout;
  layout->setSpacing(0);
  layout->setMargin(0);
  layout->addWidget(createCellWidget(tr("Patch Set"), DiagramItem::PatchSet), 0, 0, Qt::AlignTop);
  //layout->addWidget(createCellWidget(tr("Patch Sets"), DiagramItem::PatchSets), 1, 0);
  //layout->addWidget(createCellWidget(tr("Sort"), DiagramItem::Sort), 2, 0);
  layout->addWidget(createOperatorsWidget(tr("Operator")), 0, 1, Qt::AlignTop);
  //layout->addWidget(createCellWidget(tr("IN"), DiagramItem::OperatorIN), 0, 1);
  //layout->addWidget(createCellWidget(tr("NOT IN"), DiagramItem::OperatorNOTIN), 1, 1);
  //layout->addWidget(createCellWidget(tr("THRESHOLD"), DiagramItem::OperatorTHRESHOLD), 1, 0);
  //layout->addWidget(createCellWidget(tr("Op AND"), DiagramItem::OperatorAND), 2, 0);
  //layout->addWidget(createCellWidget(tr("Op OR"), DiagramItem::OperatorOR), 2, 1);

  //  layout->setRowStretch(3, 10);
  // layout->setColumnStretch(2, 10);

  itemWidget = new QWidget;
  itemWidget->setLayout(layout);
}

void QueryBuilder::createActions() {
  toFrontAction = new QAction(QIcon("resources/images/bringtofront.png"),
                              tr("Bring to &front"), this);
  toFrontAction->setShortcut(tr("Ctrl+F"));
  toFrontAction->setStatusTip(tr("Bring object to front"));
  connect(toFrontAction, SIGNAL(triggered()), this, SLOT(bringToFront()));

  sendBackAction = new QAction(QIcon("resources/images/sendtoback.png"), tr("Send to &back"), this);
  sendBackAction->setShortcut(tr("Ctrl+B"));
  sendBackAction->setStatusTip(tr("Send object to back"));
  connect(sendBackAction, SIGNAL(triggered()), this, SLOT(sendToBack()));

  deleteAction = new QAction(QIcon("resources/images/delete.png"), tr("&Delete"), this);
  deleteAction->setShortcut(tr("Delete"));
  deleteAction->setStatusTip(tr("Delete object"));
  connect(deleteAction, SIGNAL(triggered()), this, SLOT(deleteItem()));

  exitAction = new QAction(tr("E&xit"), this);
  exitAction->setShortcuts(QKeySequence::Quit);
  exitAction->setStatusTip(tr("Quit"));
  connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

  boldAction = new QAction(tr("Bold"), this);
  boldAction->setCheckable(true);
  QPixmap pixmap("resources/images/bold.png");
  boldAction->setIcon(QIcon(pixmap));
  boldAction->setShortcut(tr("Ctrl+B"));
  connect(boldAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

  italicAction = new QAction(QIcon("resources/images/italic.png"), tr("Italic"), this);
  italicAction->setCheckable(true);
  italicAction->setShortcut(tr("Ctrl+I"));
  connect(italicAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

  underlineAction = new QAction(QIcon("resources/images/underline.png"), tr("Underline"), this);
  underlineAction->setCheckable(true);
  underlineAction->setShortcut(tr("Ctrl+U"));
  connect(underlineAction, SIGNAL(triggered()), this, SLOT(handleFontChange()));

  aboutAction = new QAction(tr("Show commands"), this);
  aboutAction->setShortcut(tr("Ctrl+B"));
  connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

  /*
  precompute_histogram_from_image_source_action_ = new QAction(
    tr("&Precompute"), this);
  precompute_histogram_from_image_source_action_->setStatusTip(tr("Precompute from an image source"));
  connect(precompute_histogram_from_image_source_action_, SIGNAL(triggered()), this,
          SLOT(UpdatePatchSetFromNewImageSource()));

  add_patches_to_histogram_from_selection_action_ = new QAction(tr("Do Selection"), this);
  add_patches_to_histogram_from_selection_action_->setStatusTip(
    tr("Add Patches by Selection on Tiled Image"));
  connect(add_patches_to_histogram_from_selection_action_, SIGNAL(triggered()), this,
          SLOT(AddPatchesToHistogramFromSelection()));

  save_patch_set_object_to_disk_action_ = new QAction(
    tr("&Save"), this);
  save_patch_set_object_to_disk_action_->setStatusTip(tr("Save to Disk"));
  connect(save_patch_set_object_to_disk_action_, SIGNAL(triggered()), this,
          SLOT(SavePatchSetObjectToDisk()));

  load_histogram_from_disk_action_ = new QAction(
    tr("&Load"), this);
  load_histogram_from_disk_action_->setStatusTip(tr("Load from Disk"));
  connect(load_histogram_from_disk_action_, SIGNAL(triggered()), this,
          SLOT(LoadHistogramFromDisk()));

  set_selected_item_as_active_patch_set_ = new QAction(
    tr("&Explore"), this);
  set_selected_item_as_active_patch_set_->setStatusTip(tr("Explore Patch Set"));
  connect(set_selected_item_as_active_patch_set_, SIGNAL(triggered()), this,
          SLOT(SetSelectedItemAsActivePatchSet()));

  open_tiled_image_action_ = new QAction(tr("&Open tiled image"), this);
  open_tiled_image_action_->setShortcuts(QKeySequence::Open);
  open_tiled_image_action_->setStatusTip(tr("Open tiled image"));
  connect(open_tiled_image_action_, SIGNAL(triggered()), this,
          SLOT(OpenTiledImage()));

  open_image_db_action_ = new QAction(tr("&Open image db"), this);
  open_image_db_action_->setShortcuts(QKeySequence::Open);
  open_image_db_action_->setStatusTip(tr("Open image db"));
  connect(open_image_db_action_, SIGNAL(triggered()), this,
	  SLOT(OpenImageDB()));

//   compute_histogram_action_ = new QAction(tr("&Compute Histogram..."), this);
//   compute_histogram_action_->setStatusTip(tr("Compute histogram of loaded tiled image"));
//   connect(compute_histogram_action_, SIGNAL(triggered()), this, SLOT(ComputeHistogram()));

  operator_in_notin_compute_action_ = new QAction(tr("&Compute"), this);
  operator_in_notin_compute_action_->setStatusTip(tr("Compute"));
  connect(operator_in_notin_compute_action_, SIGNAL(triggered()), this,
          SLOT(OperatorInAndNotInCompute()));

  operator_threshold_compute_action_ = new QAction(tr("&Compute"), this);
  operator_threshold_compute_action_->setStatusTip(tr("Compute"));
  connect(operator_threshold_compute_action_, SIGNAL(triggered()), this,
          SLOT(OperatorThresholdCompute()));

  operator_and_or_compute_action_ = new QAction(tr("&Compute"), this);
  operator_and_or_compute_action_->setStatusTip(tr("Compute"));
  connect(operator_and_or_compute_action_, SIGNAL(triggered()), this,
          SLOT(OperatorAndAndOrCompute()));
  
  select_operator_threshold_action_ = new QAction(tr("Threshold"), this);
  connect(select_operator_threshold_action_, SIGNAL(triggered()), this,
          SLOT(SelectOperatorTypeThreshold()));

  select_operator_in_action_ = new QAction(tr("In"), this);
  connect(select_operator_in_action_, SIGNAL(triggered()), this,
          SLOT(SelectOperatorTypeIn()));

  select_operator_notin_action_ = new QAction(tr("!In"), this);
  connect(select_operator_notin_action_, SIGNAL(triggered()), this,
          SLOT(SelectOperatorTypeNotIn()));

  select_operator_and_action_ = new QAction(tr("And"), this);
  connect(select_operator_and_action_, SIGNAL(triggered()), this,
          SLOT(SelectOperatorTypeAnd()));

  select_operator_or_action_ = new QAction(tr("Or"), this);
  connect(select_operator_or_action_, SIGNAL(triggered()), this,
          SLOT(SelectOperatorTypeOr()));

  // Create set of check boxes to select thresholding type:
  QGroupBox *group_box_threshold_type_ = new QGroupBox(tr("Threshold type/s:"));
  check_box_less_ = new QCheckBox(tr("<"));
  check_box_equal_ = new QCheckBox(tr("=="));
  check_box_greater_ = new QCheckBox(tr(">"));

  connect(check_box_less_, SIGNAL(clicked()), this, SLOT(SetThresholdOperator()));
  connect(check_box_equal_, SIGNAL(clicked()), this, SLOT(SetThresholdOperator()));
  connect(check_box_greater_, SIGNAL(clicked()), this, SLOT(SetThresholdOperator()));
  check_box_greater_->setChecked(true);

  QVBoxLayout *vbox_threshold_type = new QVBoxLayout;
  vbox_threshold_type->addWidget(check_box_less_);
  vbox_threshold_type->addWidget(check_box_equal_);
  vbox_threshold_type->addWidget(check_box_greater_);
  vbox_threshold_type->addStretch(1);
  group_box_threshold_type_->setLayout(vbox_threshold_type);
  set_threshold_operator_type_action_ = new QWidgetAction(this);
  set_threshold_operator_type_action_->setDefaultWidget(group_box_threshold_type_);

  thresholdLineEdit = new QLineEdit(tr("50"));
  thresholdLineEdit->setMaxLength(7);
  thresholdLineEdit->setFixedWidth(50);
  connect(thresholdLineEdit, SIGNAL(editingFinished()),
          this, SLOT(OperatorThresholdChanged()));
  QLabel *threshold_label = new QLabel(tr("Threshold %:  "));
  QHBoxLayout* threshold_layout = new QHBoxLayout();
  threshold_layout->addWidget(threshold_label);
  threshold_layout->addWidget(thresholdLineEdit);
  QWidget *threshold_widget = new QWidget(this);
  threshold_widget->setLayout(threshold_layout);
  set_threshold_operator_value_action_ = new QWidgetAction(this);
  set_threshold_operator_value_action_->setDefaultWidget(threshold_widget);

  process_image_db_source_action_ = new QAction(tr("&Process image DB source"), this);
  process_image_db_source_action_->setStatusTip(tr("Process Image DB Source"));
  connect(process_image_db_source_action_, SIGNAL(triggered()), this, SLOT(ProcessImageDBSource()));

  write_image_masks_action_ = new QAction(tr("Write image masks"), this);
  write_image_masks_action_->setStatusTip(tr("Write image mask"));
  connect(write_image_masks_action_, SIGNAL(triggered()), this, SLOT(WriteImageMasks()));

  write_histogram_counts_action_ = new QAction(tr("Write histogram counts"), this);
  write_histogram_counts_action_->setStatusTip(tr("Write histogram counts to disk"));
  connect(write_histogram_counts_action_, SIGNAL(triggered()), this, SLOT(WriteHistogramCounts()));

//   process_image_source_list_action_ = new QAction(tr("&Process image source list"), this);
//   process_image_source_list_action_->setStatusTip(tr("Process Image Source into Patch Set"));
//   connect(process_image_source_list_action_, SIGNAL(triggered()), this, SLOT(ProcessImageSourceList()));

  // Create slider for patch pointers size:
  QWidget* patch_pointers_size_slider_widget = new QWidget(this);
  QHBoxLayout* patch_pointers_size_slider_layout = new QHBoxLayout();
  QLabel *patch_pointers_size_slider_label = new QLabel(tr("Pointers size"));
  patch_pointers_size_slider_layout->addWidget(patch_pointers_size_slider_label);
  QSlider *slider_patch_pointers_size_ = new QSlider(Qt::Horizontal, this);
  slider_patch_pointers_size_->setMinimum(0);
  slider_patch_pointers_size_->setMaximum(100);
  slider_patch_pointers_size_->setSingleStep(10);
  connect(slider_patch_pointers_size_, SIGNAL(valueChanged(int)), this,
          SLOT(SetPatchPointersSize(int)));
  patch_pointers_size_slider_layout->addWidget(slider_patch_pointers_size_);
  patch_pointers_size_slider_widget->setLayout(patch_pointers_size_slider_layout);
  adjust_patch_pointers_size_action_ = new QWidgetAction(this);
  adjust_patch_pointers_size_action_->setDefaultWidget(patch_pointers_size_slider_widget);

  // Create slider for selection depth:
  QWidget* selection_depth_slider_widget = new QWidget(this);
  QHBoxLayout* selection_depth_slider_layout = new QHBoxLayout();
  QLabel *selection_depth_slider_label = new QLabel(tr("Selection depth: "));
  selection_depth_slider_layout->addWidget(selection_depth_slider_label);
  selection_depth_slider_ = new QSlider(Qt::Horizontal, this);
  selectionDepthLabel = new QLabel(tr("0"));
  selection_depth_slider_->setMinimum(0);
  selection_depth_slider_->setMaximum(12);
  selection_depth_slider_->setSingleStep(1);
  selection_depth_slider_->setTickInterval(1);
  selection_depth_slider_->setTickPosition(QSlider::TicksBelow);
  connect(selection_depth_slider_, SIGNAL(valueChanged(int)), this,
          SLOT(SetSelectionDepth(int)));
  selection_depth_slider_layout->addWidget(selectionDepthLabel);
  selection_depth_slider_layout->addWidget(selection_depth_slider_);
  selection_depth_slider_widget->setLayout(selection_depth_slider_layout);
  adjust_selection_depth_action_ = new QWidgetAction(this);
  adjust_selection_depth_action_->setDefaultWidget(selection_depth_slider_widget);

  // Create slider for look ahead depth:
  QWidget* lookahead_depth_slider_widget = new QWidget(this);
  QHBoxLayout* lookahead_depth_slider_layout = new QHBoxLayout();
  QLabel *lookahead_depth_slider_label = new QLabel(tr("Look ahead depth: "));
  lookahead_depth_slider_layout->addWidget(lookahead_depth_slider_label);
  lookahead_depth_slider_ = new QSlider(Qt::Horizontal, this);
  lookaheadDepthLabel = new QLabel(tr("0"));
  lookahead_depth_slider_->setMinimum(0);
  lookahead_depth_slider_->setMaximum(12);
  lookahead_depth_slider_->setSingleStep(1);
  lookahead_depth_slider_->setTickInterval(1);
  lookahead_depth_slider_->setTickPosition(QSlider::TicksBelow);
  connect(lookahead_depth_slider_, SIGNAL(valueChanged(int)), this,
          SLOT(SetLookaheadDepth(int)));
  lookahead_depth_slider_layout->addWidget(lookaheadDepthLabel);
  lookahead_depth_slider_layout->addWidget(lookahead_depth_slider_);
  lookahead_depth_slider_widget->setLayout(lookahead_depth_slider_layout);
  adjust_lookahead_depth_action_ = new QWidgetAction(this);
  adjust_lookahead_depth_action_->setDefaultWidget(lookahead_depth_slider_widget);
  lookahead_depth_slider_->setValue(2);

  // Create set of check boxes to select visible patch pointers:
  QGroupBox *group_box = new QGroupBox(tr("Patch pointers"));
  displayCoarsePatchPointersCheckBox = new QCheckBox(tr("Show coarse levels"));
  SetCheckBoxColor(displayCoarsePatchPointersCheckBox, QColor(44, 123, 182, 220));
  displayCurrentPatchPointersCheckBox = new QCheckBox(tr("Show current level"));
  SetCheckBoxColor(displayCurrentPatchPointersCheckBox, QColor(255, 255, 0, 220));
  displayFinePatchPointersCheckBox = new QCheckBox(tr("Show fine levels"));
  SetCheckBoxColor(displayFinePatchPointersCheckBox, QColor(215, 25, 28, 100));

  connect(displayCoarsePatchPointersCheckBox, SIGNAL(clicked(bool)), this,
          SLOT(SetCoarseLevelPatchPointersVisibility(bool)));
  connect(displayCurrentPatchPointersCheckBox, SIGNAL(clicked(bool)), this,
          SLOT(SetCurrentLevelPatchPointersVisibility(bool)));
  connect(displayFinePatchPointersCheckBox, SIGNAL(clicked(bool)), this,
          SLOT(SetFineLevelPatchPointersVisibility(bool)));
  displayCoarsePatchPointersCheckBox->setChecked(true);
  displayCurrentPatchPointersCheckBox->setChecked(true);
  displayFinePatchPointersCheckBox->setChecked(true);
  
  QVBoxLayout *vbox = new QVBoxLayout;
  vbox->addWidget(displayCoarsePatchPointersCheckBox);
  vbox->addWidget(displayCurrentPatchPointersCheckBox);
  vbox->addWidget(displayFinePatchPointersCheckBox);
 // vbox->addStretch(1);
  group_box->setLayout(vbox);
  set_visible_patch_pointers_action_ = new QWidgetAction(this);
  set_visible_patch_pointers_action_->setDefaultWidget(group_box);

  QHBoxLayout *resize_image_layout = new QHBoxLayout;
  check_box_resize_images_ = new QCheckBox(tr("Resize images to patch size"));
  QLabel *resize_image_label = new QLabel(tr("Resize images to patch size"));
  check_box_resize_images_->setChecked(false);
  resize_image_layout->addWidget(check_box_resize_images_);
  resize_image_layout->addWidget(resize_image_label);
  QWidget* resize_image_widget = new QWidget(this);
  resize_image_widget->setLayout(resize_image_layout);
  set_resize_images_action_ = new QWidgetAction(this);
  set_resize_images_action_->setDefaultWidget(check_box_resize_images_);

  QHBoxLayout *process_multires_images_layout = new QHBoxLayout;
  check_box_process_multires_images_ = new QCheckBox(tr("Process multires. images"));
  QLabel *process_multires_images_label = new QLabel(tr("Process images in multiresolution up to patch size"));
  check_box_process_multires_images_->setChecked(false);
  process_multires_images_layout->addWidget(check_box_process_multires_images_);
  process_multires_images_layout->addWidget(process_multires_images_label);
  QWidget* process_multires_images_widget = new QWidget(this);
  process_multires_images_widget->setLayout(process_multires_images_layout);
  set_process_multires_images_action_ = new QWidgetAction(this);
  set_process_multires_images_action_->setDefaultWidget(check_box_process_multires_images_);

  // Switch for GPU
  check_box_gpu_switch_ = new QCheckBox(tr("Use GPU"));
  check_box_gpu_switch_->setChecked(false);
  set_gpu_switch_action_ = new QWidgetAction(this);
  set_gpu_switch_action_->setDefaultWidget(check_box_gpu_switch_);

  // Widget for adjusting patch size
  patchSizeLineEdit = new QLineEdit(tr("64"));
  patchSizeLineEdit->setMaxLength(4);
  patchSizeLineEdit->setFixedWidth(40);
  //   connect(patchSizeLineEdit, SIGNAL(editingFinished()),
  //           this, SLOT(OperatorPatchSizeChanged()));
  QLabel *patch_size_label = new QLabel(tr("Square patch size:  "));
  QHBoxLayout* patch_size_layout = new QHBoxLayout();
  patch_size_layout->addWidget(patch_size_label);
  patch_size_layout->addWidget(patchSizeLineEdit);
  QWidget *patch_size_widget = new QWidget(this);
  patch_size_widget->setLayout(patch_size_layout);
  set_patch_size_action_ = new QWidgetAction(this);
  set_patch_size_action_->setDefaultWidget(patch_size_widget);

  // Widget for adjusting patch sampling interval
  patchSamplingIntervalLineEdit = new QLineEdit(tr("32"));
  patchSamplingIntervalLineEdit->setMaxLength(4);
  patchSamplingIntervalLineEdit->setFixedWidth(40);
  QLabel *patch_sampling_interval_label = new QLabel(tr("Square patch sampling interval:  "));
  QHBoxLayout* patch_sampling_interval_layout = new QHBoxLayout();
  patch_sampling_interval_layout->addWidget(patch_sampling_interval_label);
  patch_sampling_interval_layout->addWidget(patchSamplingIntervalLineEdit);
  QWidget *patch_sampling_interval_widget = new QWidget(this);
  patch_sampling_interval_widget->setLayout(patch_sampling_interval_layout);
  set_patch_sampling_interval_action_ = new QWidgetAction(this);
  set_patch_sampling_interval_action_->setDefaultWidget(patch_sampling_interval_widget);
  
  // Combo box for feature space grid type.
  featureSpaceGridTypeCombo = new QComboBox;
  QStringList grid_types;
  for (int grid_index = 0; grid_index < int(BinningTypeNames.size()); ++grid_index) {
    grid_types << tr(BinningTypeNames[grid_index].c_str());
  }
  featureSpaceGridTypeCombo->addItems(grid_types);
  featureSpaceGridTypeCombo->setCurrentIndex(0);
  connect(featureSpaceGridTypeCombo, SIGNAL(currentIndexChanged(int)),
          this, SLOT(FeatureSpaceGridTypeChanged(int)));
  QWidget* grid_widget = new QWidget(this);
  QHBoxLayout* grid_layout = new QHBoxLayout();
  QLabel *grid_label = new QLabel(tr("Feature space grid"));
  grid_layout->addWidget(grid_label);
  grid_layout->addWidget(featureSpaceGridTypeCombo);
  grid_widget->setLayout(grid_layout);
  set_feature_space_grid_type_action_ = new QWidgetAction(this);
  set_feature_space_grid_type_action_->setDefaultWidget(grid_widget);

  // Combo box for patch sampling type.
  patchSamplingTypeCombo = new QComboBox;
  QStringList patch_sampling_types;
  for (int patch_sampling_index = 0; patch_sampling_index < int(PatchSamplingTypeNames.size()); 
       ++patch_sampling_index) {
    patch_sampling_types << tr(PatchSamplingTypeNames[patch_sampling_index].c_str());
  }
  patchSamplingTypeCombo->addItems(patch_sampling_types);
  patchSamplingTypeCombo->setCurrentIndex(0);
  connect(patchSamplingTypeCombo, SIGNAL(currentIndexChanged(int)),
          this, SLOT(PatchSamplingTypeChanged(int)));
  QWidget* patch_sampling_widget = new QWidget(this);
  QHBoxLayout* patch_sampling_layout = new QHBoxLayout();
  QLabel *patch_sampling_label = new QLabel(tr("Patch sampling"));
  patch_sampling_layout->addWidget(patch_sampling_label);
  patch_sampling_layout->addWidget(patchSamplingTypeCombo);
  patch_sampling_widget->setLayout(patch_sampling_layout);
  set_patch_sampling_type_action_ = new QWidgetAction(this);
  set_patch_sampling_type_action_->setDefaultWidget(patch_sampling_widget);
  
  // Combo box for patch explorer bin sort type
  patchExplorerBinSortTypeCombo = new QComboBox;
  QStringList bin_sort_types;
  for (int sort_type_index = 0; sort_type_index < int(BinDataSortType_NUMPATCHES_NONE); 
       ++sort_type_index) {
    bin_sort_types << tr(BinDataSortTypeNames[sort_type_index].c_str());
  }
  patchExplorerBinSortTypeCombo->addItems(bin_sort_types);
  patchExplorerBinSortTypeCombo->setCurrentIndex(0);
  connect(patchExplorerBinSortTypeCombo, SIGNAL(currentIndexChanged(int)),
          this, SLOT(PatchExplorerBinSortTypeChanged(int)));
//   connect(patchExplorerBinSortTypeCombo, SIGNAL(aboutToShow()),
//           this, SLOT(PatchExplorerBinSortTypeChanged(int)));
  QWidget* bin_sort_type_widget = new QWidget(this);
  QHBoxLayout* bin_sort_type_layout = new QHBoxLayout();
  QLabel *bin_sort_type_label = new QLabel(tr("Sort bins"));
  bin_sort_type_layout->addWidget(bin_sort_type_label);
  bin_sort_type_layout->addWidget(patchExplorerBinSortTypeCombo);
  bin_sort_type_widget->setLayout(bin_sort_type_layout);
  set_patch_explorer_bin_sort_type_action_ = new QWidgetAction(this);
  set_patch_explorer_bin_sort_type_action_->setDefaultWidget(bin_sort_type_widget);

  // Combo box for patch explorer source sort type
  sourceSortTypeCombo = new QComboBox;
  QStringList source_sort_types;
  for (int source_sort_type_index = 0; source_sort_type_index < int(HistogramObjectSortType_INVALID);
       ++source_sort_type_index) {
    source_sort_types << tr(HistogramObjectSortTypeNames[source_sort_type_index].c_str());
  }
  sourceSortTypeCombo->addItems(source_sort_types);
  sourceSortTypeCombo->setCurrentIndex(0);
  connect(sourceSortTypeCombo, SIGNAL(currentIndexChanged(int)),
          this, SLOT(PatchExplorerSourceSortTypeChanged(int)));
  QWidget* source_sort_type_widget = new QWidget(this);
  QHBoxLayout* source_sort_type_layout = new QHBoxLayout();
  QLabel *source_sort_type_label = new QLabel(tr("Sort sources"));
  source_sort_type_layout->addWidget(source_sort_type_label);
  source_sort_type_layout->addWidget(sourceSortTypeCombo);
  source_sort_type_widget->setLayout(source_sort_type_layout);
  set_source_sort_type_action_ = new QWidgetAction(this);
  set_source_sort_type_action_->setDefaultWidget(source_sort_type_widget);

  // For selecting feature space of threshold operator
  featureTypeComboThr = new QComboBox;
  QStringList features;
  for (int feature_index = 0; feature_index < int(FeaturePropertiesList.size()); ++feature_index) {
    features << tr(FeaturePropertiesList[feature_index].name.c_str());
  }
  featureTypeComboThr->addItems(features);
  featureTypeComboThr->setCurrentIndex(0);
  connect(featureTypeComboThr, SIGNAL(currentIndexChanged(int)),
          this, SLOT(OperatorFeatureTypeChanged(int)));
  set_thr_operator_feature_space_action_ = new QWidgetAction(this);

  //   connect(featureTypeCombo, SIGNAL(aboutTob(int)),
  //           this, SLOT(FeatureTypeChanged(int)));
  //   set_operator_feature_space_action_ = new QWidgetAction(this);

  QLabel *features_label = new QLabel(tr("Feature: "));
  QHBoxLayout* fs_layout = new QHBoxLayout();
  fs_layout->addWidget(features_label);
  fs_layout->addWidget(featureTypeComboThr);
  QWidget *fs_widget = new QWidget(this);
  fs_widget->setLayout(fs_layout);
  set_thr_operator_feature_space_action_->setDefaultWidget(fs_widget);

  // For selecting feature space of in and not in operator
  featureTypeComboBinInt = new QComboBox;
  QStringList features_binint;
  for (int feature_index = 0; feature_index < int(FeaturePropertiesList.size()); ++feature_index) {
    features_binint << tr(FeaturePropertiesList[feature_index].name.c_str());
  }
  featureTypeComboBinInt->addItems(features_binint);
  featureTypeComboBinInt->setCurrentIndex(0);
  connect(featureTypeComboBinInt, SIGNAL(currentIndexChanged(int)),
          this, SLOT(OperatorFeatureTypeChanged(int)));
  QLabel *features_label_binint = new QLabel(tr("Feature:"));
  QHBoxLayout* fs_layout_binint = new QHBoxLayout();
  fs_layout_binint->addWidget(features_label_binint);
  fs_layout_binint->addWidget(featureTypeComboBinInt);
  QWidget *fs_widget_binint = new QWidget(this);
  fs_widget_binint->setLayout(fs_layout_binint);
  set_binintersection_operator_feature_space_action_ = new QWidgetAction(this);
  set_binintersection_operator_feature_space_action_->setDefaultWidget(fs_widget_binint);

  // Sigma editor for operator
  operatorSigmaLineEdit = new QLineEdit(tr("2"));
  operatorSigmaLineEdit->setMaxLength(5);
  operatorSigmaLineEdit->setFixedWidth(50);
  connect(operatorSigmaLineEdit, SIGNAL(editingFinished()),
          this, SLOT(OperatorSigmaChanged()));
  QLabel *op_sigma_label = new QLabel(tr("Operator Sigma:"));
  QHBoxLayout* operator_sigma_layout = new QHBoxLayout();
  operator_sigma_layout->addWidget(op_sigma_label);
  operator_sigma_layout->addWidget(operatorSigmaLineEdit);
  QWidget *op_sigma_widget = new QWidget(this);
  op_sigma_widget->setLayout(operator_sigma_layout);
  set_operator_sigma_action_ = new QWidgetAction(this);
  set_operator_sigma_action_->setDefaultWidget(op_sigma_widget);

  spatialQuantizationLineEdit = new QLineEdit(tr("128"));
  spatialQuantizationLineEdit->setMaxLength(5);
  operatorSigmaLineEdit->setFixedWidth(50);
  connect(spatialQuantizationLineEdit, SIGNAL(editingFinished()),
          this, SLOT(SpatialQuantizationChanged()));
  QLabel *spatial_quantization_label = new QLabel(tr("Spatial quantization:"));
  QHBoxLayout* spatial_quantization_layout = new QHBoxLayout();
  spatial_quantization_layout->addWidget(spatial_quantization_label);
  spatial_quantization_layout->addWidget(spatialQuantizationLineEdit);
  QWidget *spatial_quantization_widget = new QWidget(this);
  spatial_quantization_widget->setLayout(spatial_quantization_layout);
  set_pe_spatial_quantization_action_ = new QWidgetAction(this);
  set_pe_spatial_quantization_action_->setDefaultWidget(spatial_quantization_widget);


  // Combo box for showing patch list options.
  displayQuantizedPatchLists = new QComboBox(this);
  QStringList display_patches;
  display_patches << tr("quantized only") << tr("all");
  displayQuantizedPatchLists->addItems(display_patches);
  displayQuantizedPatchLists->setCurrentIndex(1);
  connect(displayQuantizedPatchLists, SIGNAL(currentIndexChanged(int)),
          this, SLOT(DisplayPatchesChanged(int)));
  QWidget* display_patches_widget = new QWidget(this);
  QHBoxLayout* display_patches_layout = new QHBoxLayout();
  QLabel *display_patches_label = new QLabel(tr("Show patches:"));
  display_patches_layout->addWidget(display_patches_label);
  display_patches_layout->addWidget(displayQuantizedPatchLists);
  display_patches_widget->setLayout(display_patches_layout);
  
  set_visible_patch_display_action_ = new QWidgetAction(this);
  set_visible_patch_display_action_->setDefaultWidget(display_patches_widget);

  check_box_use_cache_ = new QCheckBox(tr("Use Cache"));
  check_box_use_cache_->setChecked(false);
  use_cache_action_ = new QWidgetAction(this);
  use_cache_action_->setDefaultWidget(check_box_use_cache_);

  check_box_histogram_use_percent_ = new QCheckBox(tr("Show percentage"));
  check_box_histogram_use_percent_->setChecked(true);
  histogram_use_percent_action_ = new QWidgetAction(this);
  histogram_use_percent_action_->setDefaultWidget(check_box_histogram_use_percent_);
  connect(histogram_use_percent_action_, SIGNAL(triggered()),
          this, SLOT(HistogramPercentageChanged()));

  // For selecting feature space of patch explorer
  explorerFeatureTypeCombo = new QComboBox;
  QStringList features_explorer;
  for (int feature_index = 0; feature_index < int(FeaturePropertiesList.size()); ++feature_index) {
    features_explorer << tr(FeaturePropertiesList[feature_index].name.c_str());
  }
  explorerFeatureTypeCombo->addItems(features_explorer);
  explorerFeatureTypeCombo->setCurrentIndex(0);
  connect(explorerFeatureTypeCombo, SIGNAL(currentIndexChanged(int)),
          this, SLOT(ExplorerFeatureTypeChanged(int)));
  QLabel *features_label_explorer = new QLabel(tr("Explore:"));
  QHBoxLayout* fs_layout_explorer = new QHBoxLayout();
  fs_layout_explorer->addWidget(features_label_explorer);
  fs_layout_explorer->addWidget(explorerFeatureTypeCombo);
  fs_widget_explorer_ = new QWidget(this);
  fs_widget_explorer_->setLayout(fs_layout_explorer);

  */

  /*
  save_histogram_action_ = new QAction(tr("&Save Histogram..."), this);
  save_histogram_action_->setStatusTip(tr("Save histogram to disk"));
  connect(save_histogram_action_, SIGNAL(triggered()), this, SLOT(SaveHistogram()));

  load_histogram_action_ = new QAction(tr("&Load Histogram..."), this);
  load_histogram_action_->setStatusTip(tr("Load histogram from disk"));
  connect(load_histogram_action_, SIGNAL(triggered()), this, SLOT(LoadHistogram()));

  attach_histogram_to_patch_explorer_action_ = new QAction(tr("&Attach Histogram..."), this);
  attach_histogram_to_patch_explorer_action_->setStatusTip(tr(
  "Attach computed histogram to Patch Explorer"));
  connect(attach_histogram_to_patch_explorer_action_, SIGNAL(triggered()),
  this, SLOT(AttachHistogramToPatchExplorer()));
  */
}

void QueryBuilder::createMenus() {

  //fileMenu = menuBar()->addMenu(tr("&File"));
  /*
  fileMenu->addAction(open_tiled_image_action_);  
  fileMenu->addAction(open_image_db_action_);
  fileMenu->addAction(write_image_masks_action_);
  fileMenu->addAction(process_image_db_source_action_);
  fileMenu->addAction(set_resize_images_action_);
  //fileMenu->addAction(set_process_multires_images_action_);
  */

#ifdef GIGAPATCHEXPLORER_USE_CUDA
  //fileMenu->addAction(set_gpu_switch_action_);
#endif // GIGAPATCHEXPLORER_USE_CUDA
  //fileMenu->addAction(use_cache_action_);
  //fileMenu->addAction(exitAction);
  /*
  itemMenu = menuBar()->addMenu(tr("&Item"));
  itemMenu->addAction(deleteAction);
  itemMenu->addSeparator();
  itemMenu->addAction(toFrontAction);
  itemMenu->addAction(sendBackAction);
  */

  /*
  patchSetMenu = menuBar()->addMenu(tr("Patch Set"));
  patchSetMenu->addAction(set_selected_item_as_active_patch_set_);
  patchSetMenu->addAction(precompute_histogram_from_image_source_action_);
  patchSetMenu->addAction(add_patches_to_histogram_from_selection_action_);
  patchSetMenu->addAction(save_patch_set_object_to_disk_action_);
//  patchSetMenu->addAction(load_histogram_from_disk_action_);
  patchSetMenu->addSeparator();
  patchSetMenu->addAction(deleteAction);
  patchSetMenu->addAction(toFrontAction);
  patchSetMenu->addAction(sendBackAction);
  patchSetMenu->menuAction()->setVisible(false);
  */
  /*
  parametersMenu = menuBar()->addMenu(tr("Parameters"));
  parametersMenu->addAction(adjust_selection_depth_action_);
  parametersMenu->addAction(set_feature_space_grid_type_action_);
  parametersMenu->addAction(set_patch_sampling_type_action_);
  parametersMenu->addAction(set_patch_size_action_);
  parametersMenu->addAction(set_patch_sampling_interval_action_);
  connect(parametersMenu, SIGNAL(aboutToShow()), this,
          SLOT(UpdateParamsMenu()));

  operatorForThresholdMenu = menuBar()->addMenu(tr("Operator THRESHOLD"));
  operatorForThresholdMenu->addAction(operator_threshold_compute_action_);
  operatorForThresholdMenu->addAction(set_thr_operator_feature_space_action_);
  operatorForThresholdMenu->addAction(set_threshold_operator_value_action_);
  operatorForThresholdMenu->addAction(set_threshold_operator_type_action_);
  operatorForThresholdMenu->menuAction()->setVisible(false);
  connect(operatorForThresholdMenu, SIGNAL(aboutToShow()), this,
          SLOT(UpdateThresholdMenuFromItem()));

  operatorForInAndNotInMenu = menuBar()->addMenu(tr("Operator IN/!IN"));
  operatorForInAndNotInMenu->addAction(operator_in_notin_compute_action_);
  operatorForInAndNotInMenu->addAction(set_binintersection_operator_feature_space_action_);
  operatorForInAndNotInMenu->addAction(set_operator_sigma_action_);
  operatorForInAndNotInMenu->menuAction()->setVisible(false);
  connect(operatorForInAndNotInMenu, SIGNAL(aboutToShow()), this,
          SLOT(UpdateInAndNotInMenuFromItem()));

  operatorForAndAndOrMenu = menuBar()->addMenu(tr("Operator AND/OR"));
  operatorForAndAndOrMenu->addAction(operator_and_or_compute_action_);
  operatorForAndAndOrMenu->menuAction()->setVisible(false);

  patchExplorerMenu = menuBar()->addMenu(tr("&Patch Explorer"));
  patchExplorerMenu->addAction(set_visible_patch_pointers_action_);
  patchExplorerMenu->addAction(adjust_lookahead_depth_action_);
  patchExplorerMenu->addAction(adjust_patch_pointers_size_action_);
  patchExplorerMenu->addAction(set_pe_spatial_quantization_action_);
  patchExplorerMenu->addAction(set_visible_patch_display_action_);
  patchExplorerMenu->addAction(set_source_sort_type_action_);
  patchExplorerMenu->addAction(set_patch_explorer_bin_sort_type_action_);
  connect(patchExplorerMenu, SIGNAL(aboutToShow()), this,
          SLOT(UpdatePatchExplorerMenu()));

  histogramExplorerMenu = menuBar()->addMenu(tr("Histogram"));
  histogramExplorerMenu->addAction(histogram_use_percent_action_);
  histogramExplorerMenu->addAction(write_histogram_counts_action_);
  */

  //aboutMenu = menuBar()->addMenu(tr("&Help"));
  //aboutMenu->addAction(aboutAction);
}

void QueryBuilder::createToolbars() {
   
  QToolButton *pointerButton = new QToolButton;
  pointerButton->setCheckable(true);
  pointerButton->setChecked(true);
  pointerButton->setIcon(QIcon("resources/images/pointer.png"));
  QToolButton *linePointerButton = new QToolButton;
  linePointerButton->setCheckable(true);
  linePointerButton->setIcon(QIcon("resources/images/linepointer.png"));

  pointerTypeGroup = new QButtonGroup(this);
  pointerTypeGroup->addButton(pointerButton, int(DiagramScene::MoveItem));
  pointerTypeGroup->addButton(linePointerButton, int(DiagramScene::InsertLine));
  connect(pointerTypeGroup, SIGNAL(buttonClicked(int)),
          this, SLOT(pointerGroupClicked(int)));

  sceneScaleCombo = new QComboBox;
  QStringList scales;
  scales << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%");
  sceneScaleCombo->addItems(scales);
  sceneScaleCombo->setCurrentIndex(2);
  connect(sceneScaleCombo, SIGNAL(currentIndexChanged(QString)),
          this, SLOT(sceneScaleChanged(QString)));

  pointerToolbar = addToolBar(tr("Pointer type"));
  pointerToolbar->addWidget(pointerButton);
  pointerToolbar->addWidget(linePointerButton);
  pointerToolbar->addWidget(sceneScaleCombo);

  /*
  explorer_sigma_slider_ = new QSlider(Qt::Horizontal, this);
  int cur_feature_index = feature_space_selector_->CurrentRowIndex();
  FeatureProperties cur_feature = FeaturePropertiesList[cur_feature_index];

  explorer_sigma_slider_->setMinimum(cur_feature.min_sigma);
  explorer_sigma_slider_->setMaximum(cur_feature.max_sigma);
  int step_interval_int = int(cur_feature.max_sigma / float(constant_num_feature_sigma_steps));
  explorer_sigma_slider_->setSingleStep(step_interval_int);
  explorer_sigma_slider_->setTracking(false);
  connect(explorer_sigma_slider_, SIGNAL(sliderMoved(int)), this,
          SLOT(ComputeNewSigmaFromSlider(int)));
  connect(explorer_sigma_slider_, SIGNAL(valueChanged(int)), this,
    SLOT(ExplorerSigmaChanged(int)));
  
  featuresToolBar = addToolBar(tr("Explore Params"));
  explorerSigmaLineEdit = new QLineEdit(tr("1"));
  explorerSigmaLineEdit->setMaxLength(5);
  explorerSigmaLineEdit->setFixedWidth(50);
  explorerSigmaLineEdit->setText(QString());

  //connect(explorerSigmaLineEdit, SIGNAL(editingFinished()),
  //        this, SLOT(ExplorerSigmaChanged()));

  //QLabel *explorer_sigma_label = new QLabel(tr("Explore:"));
  //featuresToolBar->addWidget(explorer_sigma_label);
  featuresToolBar->addWidget(fs_widget_explorer_);
  featuresToolBar->addWidget(explorer_sigma_slider_);
  featuresToolBar->addWidget(explorerSigmaLineEdit);
  */

  statusToolBar = addToolBar(tr("Status"));
  status_bar = new QStatusBar;
  statusToolBar->addWidget(status_bar);
  status_bar->showMessage(tr("Ready"));

  /*
  fontColorToolButton = new QToolButton;
  fontColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
  fontColorToolButton->setMenu(createColorMenu(SLOT(textColorChanged()), Qt::black));
  textAction = fontColorToolButton->menu()->defaultAction();
  fontColorToolButton->setIcon(createColorToolButtonIcon("resources/images/textpointer.png", Qt::black));
  fontColorToolButton->setAutoFillBackground(true);
  connect(fontColorToolButton, SIGNAL(clicked()),
  this, SLOT(textButtonTriggered()));
  */

  /*
  fillColorToolButton = new QToolButton;
  fillColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
  fillColorToolButton->setMenu(createColorMenu(SLOT(itemColorChanged()), Qt::white));
  fillAction = fillColorToolButton->menu()->defaultAction();
  fillColorToolButton->setIcon(createColorToolButtonIcon(
  "resources/images/floodfill.png", Qt::white));
  connect(fillColorToolButton, SIGNAL(clicked()),
  this, SLOT(fillButtonTriggered()));

  lineColorToolButton = new QToolButton;
  lineColorToolButton->setPopupMode(QToolButton::MenuButtonPopup);
  lineColorToolButton->setMenu(createColorMenu(SLOT(lineColorChanged()), Qt::black));
  lineAction = lineColorToolButton->menu()->defaultAction();
  lineColorToolButton->setIcon(createColorToolButtonIcon(
  "resources/images/linecolor.png", Qt::black));
  connect(lineColorToolButton, SIGNAL(clicked()),
  this, SLOT(lineButtonTriggered()));

  colorToolBar = addToolBar(tr("Color"));
  //  colorToolBar->addWidget(fontColorToolButton);
  colorToolBar->addWidget(fillColorToolButton);
  colorToolBar->addWidget(lineColorToolButton);
  */

  fontCombo = new QFontComboBox();
  connect(fontCombo, SIGNAL(currentFontChanged(QFont)),
          this, SLOT(currentFontChanged(QFont)));

  fontSizeCombo = new QComboBox;
  fontSizeCombo->setEditable(true);
  for (int i = 8; i < 30; i = i + 2)
    fontSizeCombo->addItem(QString().setNum(i));
  QIntValidator *validator = new QIntValidator(2, 64, this);
  fontSizeCombo->setValidator(validator);
  connect(fontSizeCombo, SIGNAL(currentIndexChanged(QString)),
          this, SLOT(fontSizeChanged(QString)));
  /*
  textToolBar = addToolBar(tr("Font"));
  textToolBar->addWidget(fontCombo);
  textToolBar->addWidget(fontSizeCombo);
  textToolBar->addAction(boldAction);
  textToolBar->addAction(italicAction);
  textToolBar->addAction(underlineAction);
  */
}

QWidget *QueryBuilder::createCellWidget(const QString &text, DiagramItem::DiagramType type) {

  // TODO (ronell): Handle other types.
  QMenu *menu_to_use = nullptr;
  switch (type) {
    
    case DiagramItem::PatchSet:
    case DiagramItem::PatchSets:
    //  menu_to_use = patchSetMenu;
      break;
    case DiagramItem::OperatorIN:
    case DiagramItem::OperatorNOTIN:
     // menu_to_use = operatorForInAndNotInMenu;
      break;
    case DiagramItem::OperatorTHRESHOLD:
     // menu_to_use = operatorForThresholdMenu;
      break;
    case DiagramItem::OperatorOR:
      break;
    case DiagramItem::OperatorAND:
      break;
    case DiagramItem::Sort:
      break;
    case DiagramItem::Invalid:
      break;
    default:
     // menu_to_use = patchSetMenu;
      break;
  }
  DiagramItem item(type, menu_to_use);
  QIcon icon(item.image());

  QToolButton *button = new QToolButton;
  button->setIcon(icon);
  button->setIconSize(QSize(40, 40));
  button->setCheckable(true);
  buttonGroup->addButton(button, int(type));

  QGridLayout *layout = new QGridLayout;
  layout->addWidget(button, 0, 0, Qt::AlignTop);
  layout->addWidget(new QLabel(text), 1, 0, Qt::AlignTop);

  QWidget *widget = new QWidget;
  widget->setLayout(layout);

  return widget;
}

QWidget *QueryBuilder::createOperatorsWidget(const QString &text) {
  DiagramItem item(DiagramItem::OperatorIN, nullptr); // TODO change nullptr to something
  
  operator_selector_button_ = new QToolButton;
  operator_selector_button_->setIcon(QIcon(item.image()));
  operator_selector_button_->setIconSize(QSize(40, 40));
  operator_selector_button_->setCheckable(true);
  operator_selector_button_->setPopupMode(QToolButton::InstantPopup);
  QMenu *menu = new QMenu(this);
  /*
  menu->addAction(select_operator_threshold_action_);
  menu->addAction(select_operator_in_action_);
  menu->addAction(select_operator_notin_action_);
  menu->addAction(select_operator_and_action_);
  menu->addAction(select_operator_or_action_);
  */
  operator_selector_button_->setMenu(menu);
  buttonGroup->addButton(operator_selector_button_, 1);
  
  QGridLayout *layout = new QGridLayout;
  layout->addWidget(operator_selector_button_, 0, 0, Qt::AlignTop);
  operator_current_selection_label_ = new QLabel(text);
  layout->addWidget(operator_current_selection_label_, 1, 0, Qt::AlignTop);
  
  QWidget *widget = new QWidget;
  widget->setLayout(layout);

  return widget;
  
}

QMenu *QueryBuilder::createColorMenu(const char *slot, QColor defaultColor) {
  QList<QColor> colors;
  colors << Qt::black << Qt::white << Qt::red << Qt::blue << Qt::yellow;
  QStringList names;
  names << tr("green") << tr("red") << tr("white") << tr("black") << tr("blue")
    << tr("yellow");

  QMenu *colorMenu = new QMenu(this);
  for (int i = 0; i < colors.count(); ++i) {
    QAction *action = new QAction(names.at(i), this);
    action->setData(colors.at(i));
    action->setIcon(createColorIcon(colors.at(i)));
    connect(action, SIGNAL(triggered()), this, slot);
    colorMenu->addAction(action);
    if (colors.at(i) == defaultColor)
      colorMenu->setDefaultAction(action);
  }
  return colorMenu;
}

QIcon QueryBuilder::createColorToolButtonIcon(const QString &imageFile, QColor color) {
  QPixmap pixmap(50, 80);
  pixmap.fill(Qt::transparent);
  QPainter painter(&pixmap);
  QPixmap image(imageFile);
  // Draw icon centred horizontally on button.
  QRect target(4, 0, 42, 43);
  QRect source(0, 0, 42, 43);
  painter.fillRect(QRect(0, 60, 50, 80), color);
  painter.drawPixmap(target, image, source);

  return QIcon(pixmap);
}

QIcon QueryBuilder::createColorIcon(QColor color) {
  QPixmap pixmap(20, 20);
  QPainter painter(&pixmap);
  painter.setPen(Qt::NoPen);
  painter.fillRect(QRect(0, 0, 20, 20), color);

  return QIcon(pixmap);
}