/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DIAGRAMITEM_H_
#define DIAGRAMITEM_H_

#include <QGraphicsPixmapItem>
#include <QMessageBox>
#include <QList>

#include "common.h"
#include "diagramtextitem.h"

QT_BEGIN_NAMESPACE
class QPixmap;
class QGraphicsItem;
class QGraphicsScene;
class QTextEdit;
class QGraphicsSceneMouseEvent;
class QMenu;
class QGraphicsSceneContextMenuEvent;
class QMessageBox;
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QPolygonF;
QT_END_NAMESPACE

class Arrow;
class DiagramTextItem;

class DiagramItem : public QGraphicsPolygonItem {
public:
  enum {
    Type = UserType + 15
  };
  enum DiagramType {
    PatchSet, PatchSets, OperatorIN, OperatorNOTIN, OperatorTHRESHOLD,
    OperatorOR, OperatorAND, Sort, Invalid
  };
  enum ThresholdType {
    ThresholdLESS, ThresholdEQUAL, ThresholdGREATER, ThresholdINVALID
  };

  DiagramItem(DiagramType diagramType, QMenu *contextMenu, QGraphicsItem *parent = 0);
  ~DiagramItem() {
    if (myTextItemLabel != nullptr) {
      delete myTextItemLabel;
    }
  }
  void removeArrow(Arrow *arrow);
  void removeArrows();
  DiagramType diagramType() const {
    return myDiagramType;
  }
  QPolygonF polygon() const {
    return myPolygon;
  }
  void addArrow(Arrow *arrow);
  QPixmap image() const;
  int type() const Q_DECL_OVERRIDE{ return Type; }
  QString name() {
    return ((QGraphicsTextItem*)myTextItemLabel)->toPlainText();
  }
  void setTextItemLabel(DiagramTextItem* textItemLabel) {
    myTextItemLabel = textItemLabel;
    SetName(((QGraphicsTextItem*)myTextItemLabel)->toPlainText());
  }
  std::string get_name() {
    return ((QGraphicsTextItem*)myTextItemLabel)->toPlainText().toStdString();
  }
  int NameNumPixelsWide() {
    QFont font = ((QGraphicsTextItem*)myTextItemLabel)->font();
    QFontMetrics fm(font);
    QString myLabel = ((QGraphicsTextItem*)myTextItemLabel)->toPlainText();
    return fm.width(myLabel);
  }
  void SetColor(QColor new_color) {
    QBrush brush;
    brush.setColor(new_color);
    brush.setStyle(Qt::SolidPattern);
    setBrush(brush);
  }
  void SetName(QString new_label) {

    int prevPixelsWide = NameNumPixelsWide();
    ((QGraphicsTextItem*)myTextItemLabel)->setPlainText(new_label);
    int newPixelsWide = NameNumPixelsWide();

    // TODO (ronell): Resize shape.
    QTransform trans;
    
    if (boundingRect().width() < newPixelsWide + 30) {
      if (newPixelsWide == prevPixelsWide) {
        trans = trans.scale(float(newPixelsWide + 30) / boundingRect().width(), 1.0);
      } else {
        trans = trans.scale((boundingRect().width() + float(newPixelsWide - prevPixelsWide))
                            / boundingRect().width(), 1.0);
      }
      myPolygon = trans.map(myPolygon);
      this->setPolygon(myPolygon);
    }
    
    QPointF text_pos = pos() - QPointF((NameNumPixelsWide() / 2)+4, boundingRect().height() / 4);
    ((QGraphicsTextItem*)myTextItemLabel)->setPos(text_pos);
    this->update();
  }
  void SetPatchSetObjectIndex(int patchset_object_index) {
    patchset_object_index_ = patchset_object_index;
  }
  int patchset_object_index() {
    return patchset_object_index_;
  }
  //   void SetHistogramObjectIndex(int histogram_object_index) {
  //     histogram_object_index_ = histogram_object_index;
  //   }
  //   int histogram_object_index() {
  //     return histogram_object_index_;
  //   }
  //   void SetTiledImageObjectIndex(int tiled_image_object_index) {
  //     tiled_image_object_index_ = tiled_image_object_index;
  //   }
  //   int tiled_image_object_index() {
  //     return tiled_image_object_index_;
  //   }
  //   void SetComputeHistogramObjectIndex(int compute_histogram_object_index) {
  //     compute_histogram_object_index_ = compute_histogram_object_index;
  //   }
  //   int compute_histogram_object_index() {
  //     return compute_histogram_object_index_;
  //   }
  void SetFirstPatchSetObjectIndex(int pso_index) {
    first_pso_index_ = pso_index;
  }
  int first_patchset_object_index() {
    return first_pso_index_;
  }
  void SetLastPatchSetObjectIndex(int pso_index) {
    last_pso_index_ = pso_index;
  }
  int last_patchset_object_index() {
    return last_pso_index_;
  }
  void SetThresholdType(ThresholdType type, bool value) {
    if (type < ThresholdINVALID) {
      threshold_vec_[int(type)] = value;
    }
  }
  bool threshold_type(ThresholdType type) {
    if (type < ThresholdINVALID) {
      return threshold_vec_[int(type)];
    }
    return false;
  }
  void SetThresholdValue(float value) {
    threshold_value_ = value;
  }
  float threshold_value() {
    return threshold_value_;
  }
  /*
  void SetFeatureDesc(FeatureDesc feature_desc) {
    cur_feature_desc_ = feature_desc;
    if (myDiagramType != DiagramItem::PatchSet) {
      SetColor(QColor(FeaturePropertiesList[cur_feature_desc_.type].color.c_str()));
    }
  }
  FeatureDesc feature_params() {
    return cur_feature_desc_;
  }
  */
  void SetOperatorSigma(float sigma) {
    operator_sigma_ = sigma;
  }
  float operator_sigma() {
    return operator_sigma_;
  }

  QColor itemColor() const {
    switch (myDiagramType) {
      case DiagramItem::PatchSet:
        return QColor("palegreen");
        break;
      case DiagramItem::PatchSets:
        return QColor("darkturquoise");
        break;
      case DiagramItem::OperatorIN:
      case DiagramItem::OperatorNOTIN:
      case DiagramItem::OperatorTHRESHOLD:
      case DiagramItem::OperatorOR:
      case DiagramItem::OperatorAND:
        return QColor("orangered");
        break;
      case DiagramItem::Sort:
        return QColor("goldenrod");
        break;
      case DiagramItem::Invalid:
      default:
        return Qt::black;
        break;
    }
  }

  QString defaultName() const {
    switch (myDiagramType) {
      case DiagramItem::PatchSet:
        return QString("empty");
        break;
      case DiagramItem::PatchSets:
        return QString("PatchSets");
        break;
      case DiagramItem::OperatorIN:
        return QString("In");
        break;
      case DiagramItem::OperatorNOTIN:
        return QString("!In");
        break;
      case DiagramItem::OperatorTHRESHOLD:
        return QString("Thr");
        break;
      case DiagramItem::OperatorOR:
        return QString("Or");
        break;
      case DiagramItem::OperatorAND:
        return QString("And");
        break;
      case DiagramItem::Sort:
        return QString("Sort");
        break;
      case DiagramItem::Invalid:
      default:
        return QString("");
        break;
    }
  }

protected:
  void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
  QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;

private:
  DiagramType myDiagramType;
  QPolygonF myPolygon;
  QMenu *myContextMenu;
  QList<Arrow *> arrows;
  DiagramTextItem* myTextItemLabel;
  int patchset_object_index_;
  int first_pso_index_;        // When doing operator, this is the reference.
  int last_pso_index_;      // When doing operator, this is the input.
  bool threshold_vec_[3];
  float threshold_value_;
  QColor reference_operand_default_color_;
  //FeatureDesc cur_feature_desc_;
  float operator_sigma_;
};

#endif // DIAGRAMITEM_H_
