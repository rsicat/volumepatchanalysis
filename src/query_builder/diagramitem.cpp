/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "diagramitem.h"
#include "arrow.h"

#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QPainter>

DiagramItem::DiagramItem(DiagramType diagramType, QMenu *contextMenu,
                         QGraphicsItem *parent)
                         : QGraphicsPolygonItem(parent) {
  myDiagramType = diagramType;
  myContextMenu = contextMenu;
  first_pso_index_ = -1;
  last_pso_index_ = -1;
  patchset_object_index_ = -1;
  threshold_vec_[DiagramItem::ThresholdLESS] = false;
  threshold_vec_[DiagramItem::ThresholdEQUAL] = false;
  threshold_vec_[DiagramItem::ThresholdGREATER] = true;
  threshold_value_ = 100;
  reference_operand_default_color_ = QColor(215, 25, 28);
  operator_sigma_ = 0.0f;

  QPainterPath path;
  QTransform trans;
  switch (myDiagramType) {
    case Sort:
      myPolygon << QPointF(-25, 0) << QPointF(0, 25)
        << QPointF(25, 0) << QPointF(0, -25)
        << QPointF(-25, 0);
      break;
    case OperatorIN:
    case OperatorNOTIN:
    case OperatorTHRESHOLD:
    case OperatorOR:
    case OperatorAND:
      myPolygon << QPointF(-25, -25) << QPointF(25, -25)
        << QPointF(25, 25) << QPointF(-25, 25)
        << QPointF(-25, -25);
      break;
    default:  // PatchSet/s
//       myPolygon << QPointF(-40, -20) << QPointF(-17.5, 20)
//         << QPointF(40, 20) << QPointF(17.5, -20)
//         << QPointF(-40, -20);
      path.moveTo(200, 50);
      path.arcTo(150, 0, 50, 50, 0, 90);
      path.arcTo(50, 0, 50, 50, 90, 90);
      path.arcTo(50, 50, 50, 50, 180, 90);
      path.arcTo(150, 50, 50, 50, 270, 90);
      path.lineTo(200, 25);
      path.translate(QPointF(-120, -50));
      myPolygon = path.toFillPolygon();
      trans = trans.scale(0.5, 0.5);
      myPolygon = trans.map(myPolygon);     
      break;
  }
  setPolygon(myPolygon);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, true);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);

  myTextItemLabel = nullptr;
}

void DiagramItem::removeArrow(Arrow *arrow) {
  int index = arrows.indexOf(arrow);

  if (index != -1)
    arrows.removeAt(index);
}

void DiagramItem::removeArrows() {
  foreach(Arrow *arrow, arrows) {
    arrow->startItem()->removeArrow(arrow);
    arrow->endItem()->removeArrow(arrow);
    scene()->removeItem(arrow);
    delete arrow;
  }
}

void DiagramItem::addArrow(Arrow *arrow) {
  if (myDiagramType == DiagramItem::OperatorTHRESHOLD && first_pso_index_ != -1
      && arrow->endItem()->diagramType() == DiagramItem::OperatorTHRESHOLD) {
    QMessageBox warning;
    warning.setText("Only one input is allowed for this operator.");
    warning.exec();
    removeArrow(arrow);
    return;
  }

  arrows.append(arrow);
  if ( (myDiagramType == DiagramItem::OperatorIN ||
    myDiagramType == DiagramItem::OperatorNOTIN ||
    myDiagramType == DiagramItem::OperatorAND ||
    myDiagramType == DiagramItem::OperatorOR ||
    myDiagramType == DiagramItem::OperatorTHRESHOLD) &&
      arrow->startItem()->diagramType() == DiagramItem::PatchSet ) {

    int patchset_object_index = arrow->startItem()->patchset_object_index();
    if (patchset_object_index == -1) {
      printf("Warning: Invalid operand.");
      return;
    }
    if (first_pso_index_ == -1) {
      first_pso_index_ = patchset_object_index;
      arrow->setColor(reference_operand_default_color_);
    } else {
      if (last_pso_index_ != -1) {
        QMessageBox warning;
        warning.setText("The new input will be the last operand attached. The reference operand"
                        "(connected with red arrow) will remain the same.");
        warning.exec();
      }
      last_pso_index_ = patchset_object_index;
    }

    // Take the properties of the last attached item.
    DiagramItem* start_item = arrow->startItem();
  }
}

QPixmap DiagramItem::image() const {
  QPixmap pixmap(250, 250);
  pixmap.fill(Qt::transparent);
  QPainter painter(&pixmap);
  painter.setPen(QPen(itemColor(), 8));
  painter.scale(2.5, 2.5);
  painter.translate(50, 50);
  painter.drawPolyline(myPolygon);

  return pixmap;
}

void DiagramItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
  scene()->clearSelection();
  setSelected(true);
//   if (myDiagramType == DiagramItem::OperatorTHRESHOLD) {
//     myContextMenu->actionAt(QPoint(1,0));
//   }
  myContextMenu->exec(event->screenPos());
}

QVariant DiagramItem::itemChange(GraphicsItemChange change, const QVariant &value) {
  if (change == QGraphicsItem::ItemPositionChange) {
    foreach(Arrow *arrow, arrows) {
      arrow->updatePosition();
    }

    if (myTextItemLabel != nullptr) {
      QPointF text_pos = pos() - QPointF((NameNumPixelsWide()/2)+4, boundingRect().height() / 4);
      myTextItemLabel->setPos(text_pos);
    }
  } else if (change == QGraphicsItem::ItemZValueHasChanged) {
    if (myTextItemLabel != nullptr) {
      myTextItemLabel->setZValue(zValue());
      myTextItemLabel->update();
    }
  }
  return value;
}