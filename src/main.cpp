#include <QApplication>
#include <QWebSettings>

#include "mainwindow.h"

int main(int argc, char *argv[]) {
  
  QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
  
  QApplication app(argc, argv);
  app.setOrganizationName("KAUST");
  app.setApplicationName("VolumePatchAnalysis");

  // Enable web-based interfaces
  QWebSettings::globalSettings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);

  std::string data_root_dir = std::string("");
  std::string resources_root_dir = std::string("");
  if (argc > 1) {
     data_root_dir = std::string(argv[1]);
     resources_root_dir = std::string(argv[2]);
     printf("\nINFO: Using %s for data root dir.\n", data_root_dir.c_str());
     printf("\nINFO: Using %s for resources root dir.\n", resources_root_dir.c_str());
  } else {
    printf("\nWARNING: Assuming data and resources root dirs are same as binary dir.\n");
  }
  
  MainWindow main_window(data_root_dir, resources_root_dir);
  main_window.show();
  
  return app.exec();
}