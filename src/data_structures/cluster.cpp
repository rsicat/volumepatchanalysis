#include "cluster.h"

Cluster::Cluster() : 
max_patch_size_(Size3DInt(0,0,0)),
orig_cluster_id(-1) {}

Cluster::~Cluster() {}

void Cluster::AddPatch(Patch patch) {
  
  max_patch_size_.GetMax(patch.size);   // Get maximum dimension

  patches_.push_back(patch);
}
