#ifndef VOLUMEPATCHANALYSIS_CLUSTER_H_
#define VOLUMEPATCHANALYSIS_CLUSTER_H_

#include "patch.h"

class Cluster {
public:
  Cluster();
  ~Cluster();

  void AddPatch(Patch patch);

  int num_patches() {
    return int(patches_.size());
  }

  const std::vector<Patch>& patches() {
    return patches_;
  }

  const Patch& patch(int index) {
    return patches_[index];
  }

  Size3DInt max_patch_size() {
    return max_patch_size_;
  }

  int orig_cluster_id;      // this is the initial cluster id

private:
  std::vector<Patch> patches_;
  Size3DInt max_patch_size_;
};

#endif  // VOLUMEPATCHANALYSIS_CLUSTER_H_
