#ifndef VOLUMEPATCHANALYSIS_PATCH_H_
#define VOLUMEPATCHANALYSIS_PATCH_H_

#include "common.h"

// AMAL TODO: Remove redundancies
class Patch {
public:
  Patch();
  ~Patch();

  int vol_id;                       // id of the source morphology or volume
  CoordInt coord;                   // central voxel coordinate
  Size3DInt size;                   // size of patch

  InfoPerVoxel center_info;         // information about the central voxel of this patch
  PatchPerformance performance;     // patch performance measures
  VolumeParams params;               // parameters that generate source morphology  
  Size3DInt half_size;              // half size of patch; AMAL: why not just store full size?
  Size3DInt source_size;            // size of source morphology; AMAL: do we need this if we have vol_id?
};

#endif  // VOLUMEPATCHANALYSIS_PATCH_H_
