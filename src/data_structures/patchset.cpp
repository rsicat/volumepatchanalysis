#include "patchset.h"

PatchSet::PatchSet() : max_patch_size_(Size3DInt(0,0,0)) {}

PatchSet::~PatchSet() {}

void PatchSet::AddCluster(Cluster cluster) {

  max_patch_size_.GetMax(cluster.max_patch_size());

  clusters_.push_back(cluster);
}
