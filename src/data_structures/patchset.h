#ifndef VOLUMEPATCHANALYSIS_PATCHSET_H_
#define VOLUMEPATCHANALYSIS_PATCHSET_H_

#include "cluster.h"

class PatchSet {
public:
  PatchSet();
  ~PatchSet();

  void AddCluster(Cluster cluster);

  int num_clusters() {
    return int(clusters_.size());
  }

  Cluster& cluster(int index) {
    return clusters_[index];
  }

  const std::vector<Cluster>& clusters() {
    return clusters_;
  }

  Size3DInt max_patch_size() {
    return max_patch_size_;
  }

private:
  std::vector<Cluster> clusters_;
  Size3DInt max_patch_size_;        // TEMP
};

#endif  // VOLUMEPATCHANALYSIS_PATCHSET_H_
