#ifndef VOLUMEPATCHANALYSIS_COMMON_H_
#define VOLUMEPATCHANALYSIS_COMMON_H_

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#define USE_QT
#ifdef USE_QT
#include <qsize.h>
#include <QOpenGLTexture>
#endif // USE_QT

struct CoordInt {
  int x;
  int y;
  int z;

  CoordInt() : x(-1), y(-1), z(-1) {}
};

struct Size3DInt {
  int x;
  int y;
  int z;

  Size3DInt() : x(-1), y(-1), z(-1) {}
  Size3DInt(int x_, int y_, int z_) : x(x_), y(y_), z(z_) {}

  void GetMax(Size3DInt size) {
    x = std::max(x, size.x);
    y = std::max(y, size.y);
    z = std::max(z, size.z);
  }
};

// Performance metrics associated to each patch.
struct PatchPerformance {
  float performance_0;
  float performance_1;
  float performance_2;

  PatchPerformance() : performance_0(0.0f), performance_1(0.0f), performance_2(0.0f) {}
};

// Parameters that generate the morphology/volume where the patch comes from.
struct VolumeParams {
  int vol_id;
  int length;
  int width;
  int height;
  float d;
  float k1;
  float k2;
  int MC;

  VolumeParams() : length(0), width(0), height(0), d(0.0f), k1(0.0f), k2(0.0f), MC(0) {}
};

struct InfoPerVoxel {
  int x, y, z;          // voxel position
  int complimentaryPaths, interfacePerPatch;
  float distanceField, excitonDiffusionProb, photonAbsorptionProb, tortuosityValue, bottleNeck;
};

// This describes a query
struct QueryData {

  float minrange_tortuosity;
  float maxrange_tortuosity;

  float minrange_photon_absorption;
  float maxrange_photon_absorption;

  float minrange_exciton_diff;
  float maxrange_exciton_diff;

  float minrange_distance_field;
  float maxrange_distance_field;

  float minrange_bottleneck;
  float maxrange_bottleneck;

  int minrange_comp_paths;
  int maxrange_comp_paths;

  int minrange_interface_per_patch;
  int maxrange_interface_per_patch;

  // Default constructor; sets values for undefined range i.e. all values.
  QueryData() : minrange_tortuosity(-99999.0f), maxrange_tortuosity(99999.0f), 
    minrange_photon_absorption(-99999.0f), maxrange_photon_absorption(99999.0f),
    minrange_exciton_diff(-99999.0f), maxrange_exciton_diff(99999.0f),
    minrange_distance_field(-99999.0f), maxrange_distance_field(99999.0f),
    minrange_bottleneck(-99999.0f), maxrange_bottleneck(99999.0f),
    minrange_comp_paths(-99999), maxrange_comp_paths(99999),
    minrange_interface_per_patch(-999999), maxrange_interface_per_patch(999999) {}
};

typedef std::vector<float> FeatureVec;

std::string GetPatchSliceFileName(std::string slices_dir, int source_vol_id, int slice_index_y);

struct Size2DInt {
  int width;
  int height;
  Size2DInt(const int w, const int h) : width(w), height(h) {}
  Size2DInt() : width(0), height(0) {}  
};

#ifdef USE_QT
QSize Size2DIntToQSize(Size2DInt in_size);
#endif // USE_QT

struct PatchCoords {
  int image_id;
  int level;
  int x;
  int y;
  PatchCoords(int _image_id, int _level, int _x, int _y)
    : image_id(_image_id), level(_level), x(_x), y(_y) {}
  PatchCoords() : image_id(-1), level(-1), x(-1), y(-1) {}
  PatchCoords(const PatchCoords& other) : image_id(other.image_id), level(other.level),
  x(other.x), y(other.y) {}
};


struct ROI {
  int upper_left_x;
  int upper_left_y;
  Size2DInt size;
  ROI(int x, int y, int w, int h)
    : upper_left_x(x), upper_left_y(y), size(Size2DInt(w, h)) {}
};

// Computes the intersectin of two ROIs. Returns true if inputs intersect.
static bool IntersectROIs(ROI r1, ROI r2, ROI& out) {
  int xmin = std::max<int>(r1.upper_left_x, r2.upper_left_x);
  int xmax1 = r1.upper_left_x + r1.size.width;
  int xmax2 = r2.upper_left_x + r2.size.width;
  int xmax = std::min<int>(xmax1, xmax2);
  if (xmax > xmin) {
    int ymin = std::max<int>(r1.upper_left_y, r2.upper_left_y);
    int ymax1 = r1.upper_left_y + r1.size.height;
    int ymax2 = r2.upper_left_y + r2.size.height;
    int ymax = std::min<int>(ymax1, ymax2);
    if (ymax > ymin) {
      out.upper_left_x = xmin;
      out.upper_left_y = ymin;
      out.size.width = xmax - xmin;
      out.size.height = ymax - ymin;
      return true;
    }
  }
  return false;
}

inline bool AreStringsTheSame(std::string &lhs, std::string& rhs) {
  return lhs.compare(rhs) == 0;
}

struct FocusPatchParams {
  Size2DInt patch_size;
  bool enabled;
  PatchCoords coords;
};

#endif  // VOLUMEPATCHANALYSIS_COMMON_H_
