#include <QtWidgets>

#include "mainwindow.h"

MainWindow::MainWindow(std::string data_root_dir, std::string resources_root_dir) {

  data_root_dir_ = data_root_dir;
  clusters_dir_ = data_root_dir_ + std::string("/clusters/");
  std::string slices_dir = data_root_dir_ + std::string("/slices/phi0/");

  resources_root_dir_ = resources_root_dir;

  // Create application manager:
  application_manager_ = std::make_shared<ApplicationManager>(clusters_dir_);

  // Load clustering data:
  application_manager_->LoadClusterDataFromFile(clusters_dir_);

  // Create texture cache:
  const bool display_tile_filenames = false;
  texture_cache_ = std::make_shared<QTextureCache>(display_tile_filenames);

  // Create patch explorer:
  patch_explorer_ = std::make_shared<PatchExplorer>(texture_cache_.get());
  patch_explorer_->SetPatchSet(application_manager_->GetActivePatchSet(), slices_dir);
  patch_explorer_->SetApplicationManager(application_manager_);
  application_manager_->SetPatchExplorer(patch_explorer_);

  setCentralWidget(patch_explorer_.get());
  centralWidget()->setObjectName(tr("VolumePatchAnalysis"));

  // Create other interface stuff.
  CreateActions();
  CreateToolBars();
  CreateDockWindows();

  setWindowTitle(tr("Cluster Explorer"));
  setObjectName(QString("MainWindow"));
  setUnifiedTitleAndToolBarOnMac(true);

  LoadSettings();
}

MainWindow::~MainWindow() {

}

void MainWindow::closeEvent(QCloseEvent * event) {
  QSettings settings("KAUST", "VolumePatchAnalysis");
  settings.setValue("geometry", saveGeometry());
  settings.setValue("windowState", saveState());
  QMainWindow::closeEvent(event);
}

void MainWindow::LoadSettings() {
  QSettings settings("KAUST", "VolumePatchAnalysis");
  restoreGeometry(settings.value("geometry").toByteArray());
  restoreState(settings.value("windowState").toByteArray());
}

void MainWindow::ShowHelp() {
  QMessageBox::about(this, tr("VolumePatchAnalysis"),
                     tr("The <b>VolumePatchAnalysis</b> application ... "));
}

void MainWindow::CreateActions() {
  quit_action = new QAction(tr("&Quit"), this);
  quit_action->setShortcuts(QKeySequence::Quit);
  quit_action->setStatusTip(tr("Quit the application"));
  connect(quit_action, SIGNAL(triggered()), this, SLOT(close()));

  show_help_action_ = new QAction(tr("&Help"), this);
  show_help_action_->setStatusTip(tr("Show the application's help box"));
  connect(show_help_action_, SIGNAL(triggered()), this, SLOT(ShowHelp()));
}

void MainWindow::CreateToolBars() {}

void MainWindow::CreateDockWindows() {

  // Create query builder dock window:
  query_builder_ = new QueryBuilder();
  QDockWidget *dock = new QDockWidget(tr("Query Builder"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
                        Qt::BottomDockWidgetArea);
  query_builder_->setWindowFlags(Qt::Widget);  // This allows us to dock a QMainWindow.
  query_builder_->setMinimumWidth(64 * 5);
  dock->setWidget(query_builder_);
  dock->setObjectName(tr("QueryBuilderDock"));
  addDockWidget(Qt::BottomDockWidgetArea, dock);

  // Create global parallel coords interface:

  std::string global_html_filename = resources_root_dir_ + std::string("/html/global_query.html");
  global_parcoords_explorer_ = std::make_shared<ParallelCoordsExplorer>(global_html_filename, std::string("file:///") +
    clusters_dir_ + std::string("clusterPerformance_.txt"), this);
  global_parcoords_explorer_->SetApplicationManager(application_manager_);
  dock = new QDockWidget(tr("Global Parallel Coords"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
                        Qt::BottomDockWidgetArea);
  global_parcoords_explorer_->setWindowFlags(Qt::Widget);  // This allows us to dock a QMainWindow.
  global_parcoords_explorer_->setMinimumWidth(64 * 5);
  dock->setWidget(global_parcoords_explorer_.get());
  dock->setObjectName(tr("GlobalParallelCoords"));
  addDockWidget(Qt::BottomDockWidgetArea, dock);

  // Create local parallel coords interface:
  std::string local_html_filename = resources_root_dir_ + std::string("/html/index.html");
  local_parcoords_explorer_ = std::make_shared<ParallelCoordsExplorer>(local_html_filename, std::string("file:///") +
    clusters_dir_ + std::string("clusterPerformance_0.txt"), this);
  //local_parcoords_explorer_->SetPatchSet(test2);
  //local_parcoords_explorer_->SetDataSource(std::string("clusterPerformance_2.txt"));
  //application_manager_->SetGlobalParallelCoordsExplorer()
  dock = new QDockWidget(tr("Local Parallel Coords"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
                        Qt::BottomDockWidgetArea);
  global_parcoords_explorer_->setWindowFlags(Qt::Widget);  // This allows us to dock a QMainWindow.
  global_parcoords_explorer_->setMinimumWidth(64 * 5);
  dock->setWidget(local_parcoords_explorer_.get());
  dock->setObjectName(tr("LocalParallelCoords"));
  addDockWidget(Qt::BottomDockWidgetArea, dock);
  

  // Create global parallel coords interface for parameters:
  std::string global_params_html_filename = resources_root_dir_ + std::string("/html/params.html");
  global_params_explorer_ = std::make_shared<ParallelCoordsExplorer>(global_params_html_filename, std::string("file:///") +
                                                                    clusters_dir_ + std::string("clusterParams_.txt"), this);
  dock = new QDockWidget(tr("Global Params Coords"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
                        Qt::BottomDockWidgetArea);
  global_params_explorer_->setWindowFlags(Qt::Widget);  // This allows us to dock a QMainWindow.
  global_params_explorer_->setMinimumWidth(64 * 5);
  dock->setWidget(global_params_explorer_.get());
  dock->setObjectName(tr("GlobalParamsCoords"));
  addDockWidget(Qt::BottomDockWidgetArea, dock);


  // Create local parallel coords interface for parameters:
  std::string params_html_filename = resources_root_dir_ + std::string("/html/params.html");
  application_manager_->GenerateClusterParamsFile(clusters_dir_, 0); // generate the clusterParams_0.txt file.
  local_params_explorer_ = std::make_shared<ParallelCoordsExplorer>(params_html_filename, std::string("file:///") +
                                                                       clusters_dir_ + std::string("clusterParams_0.txt"), this);
  //local_parcoords_explorer_->SetPatchSet(test2);
  //local_parcoords_explorer_->SetDataSource(std::string("clusterPerformance_2.txt"));
  //application_manager_->SetGlobalParallelCoordsExplorer()
  dock = new QDockWidget(tr("Local Params Coords"), this);
  dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea |
                        Qt::BottomDockWidgetArea);
  local_params_explorer_->setWindowFlags(Qt::Widget);  // This allows us to dock a QMainWindow.
  local_params_explorer_->setMinimumWidth(64 * 5);
  dock->setWidget(local_params_explorer_.get());
  dock->setObjectName(tr("LocalParamsCoords"));
  addDockWidget(Qt::BottomDockWidgetArea, dock);


  application_manager_->SetGlobalParCoordsExplorer(global_parcoords_explorer_);
  application_manager_->SetLocalParCoordsExplorer(local_parcoords_explorer_);
  application_manager_->SetGlobalParamsExplorer(global_params_explorer_);
  application_manager_->SetLocalParamsExplorer(local_params_explorer_);
}