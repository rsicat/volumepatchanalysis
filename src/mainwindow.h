#ifndef VOLUMEPATCHANALYSIS_MAINWINDOW_H_
#define VOLUMEPATCHANALYSIS_MAINWINDOW_H_

#include <memory>

#include <QMainWindow>

#include "applicationmanager.h"
#include "explorer/parallelcoordsexplorer.h"
#include "explorer/patchexplorer.h"
#include "explorer/texturecache.h"
#include "query_builder/query_builder.h"

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QScrollArea;
class QStackedWidget;
class QOpenGLWidget;
QT_END_NAMESPACE

// Handles user inputs via the menu and manages display windows.
// Menu provides, opening, saving, closing files (images, histograms, etc.).
class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(std::string data_root_dir, std::string resources_root_dir);
  ~MainWindow();
  void closeEvent(QCloseEvent * event) Q_DECL_OVERRIDE;

signals:

private slots:
  void ShowHelp(); 

private:
  void CreateActions();
  void CreateToolBars();
  void CreateDockWindows();
  void LoadSettings();

  QToolBar *main_tool_bar_;
  QAction *show_help_action_;
  QAction *quit_action;

  QOpenGLWidget* qgl;

  std::string data_root_dir_;
  std::string resources_root_dir_;
  std::string clusters_dir_;

  std::shared_ptr<ApplicationManager> application_manager_;
  std::shared_ptr<PatchExplorer> patch_explorer_;
  QueryBuilder* query_builder_;
  std::shared_ptr<QTextureCache> texture_cache_;
  std::shared_ptr<ParallelCoordsExplorer> global_parcoords_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> local_parcoords_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> global_params_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> local_params_explorer_;
};

#endif  // VOLUMEPATCHANALYSIS_MAINWINDOW_H_
