#version 430 core

in vec2 vTexCoords;
uniform vec2 vTexCoordsScale;
uniform vec2 vTexCoordsShift;
uniform float borderPercentage;
uniform vec4 borderColor;
out vec4 fragColor;
layout(binding = 0) uniform sampler2D tileImage;

void main() {
    vec2 texCoords = (vTexCoords*vTexCoordsScale) + vTexCoordsShift;
    
    if(vTexCoords.s < borderPercentage || vTexCoords.s > 1.0f - borderPercentage) { 
      fragColor = borderColor;
    } else if(vTexCoords.t < borderPercentage || vTexCoords.t > 1.0f - borderPercentage ) {
      fragColor = borderColor;
    } else {
      fragColor = texture(tileImage, texCoords);
    }

	// Cursor for brushing

}