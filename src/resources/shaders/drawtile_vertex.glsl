#version 430 core

layout (location=0) in vec2 vertexPosition;
layout (location=1) in vec2 vertexTexCoords;
uniform mat4 matrix;
out vec2 vTexCoords;

void main(void) {
   gl_Position = matrix * vec4(vertexPosition, 0.0, 1.0);
   vTexCoords = vertexTexCoords;
}