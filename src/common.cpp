#include "common.h"

QSize Size2DIntToQSize(Size2DInt in_size) {
  QSize out_size(in_size.width, in_size.height);
  return out_size;
}

std::string GetPatchSliceFileName(std::string slices_dir, int source_vol_id, int slice_index_y) {
  std::stringstream ss;
  ss << slices_dir << "/slice_" << source_vol_id << "_" << slice_index_y << ".jpg";
  return ss.str();
}