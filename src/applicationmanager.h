#ifndef VOLUMEPATCHANALYSIS_APPLICATIONMANAGER_H_
#define VOLUMEPATCHANALYSIS_APPLICATIONMANAGER_H_

#include "data_structures/patchset.h"
#include "explorer/patchexplorer.h"

class ParallelCoordsExplorer;

class ApplicationManager {
public:
  ApplicationManager(std::string clusters_dir);
  ~ApplicationManager();

  // Load cluster data (only from start_index to end_index, inclusive) from text file.
  void LoadClusterDataFromFile(std::string filename);

  void SetGlobalParCoordsExplorer(std::shared_ptr<ParallelCoordsExplorer> g) {
    global_parcoords_explorer_ = g;
  }

  void SetLocalParCoordsExplorer(std::shared_ptr<ParallelCoordsExplorer> l) {
    local_parcoords_explorer_ = l;
  }

  void SetLocalParamsExplorer(std::shared_ptr<ParallelCoordsExplorer> p) {
    local_params_explorer_ = p;
  }

  void SetGlobalParamsExplorer(std::shared_ptr<ParallelCoordsExplorer> gp) {
    global_params_explorer_ = gp;
  }

  std::shared_ptr<PatchSet> GetActivePatchSet() {
    return patch_sets_[cur_patch_set_index_];
  }

  void SetPatchExplorer(std::shared_ptr<PatchExplorer> patch_explorer) {
    patch_explorer_ = patch_explorer;
  }

  void ExploreCluster(int cluster_index, bool force_update = false);

  void ApplyQuery(QueryData query);

  void ApplyVolumeColors(QVariantMap volume_colors);

  void ResetData();

  VolumeParams GetVolumeParams(int volume_id);
  void GenerateClusterParamsFile(std::string clusters_dir, int cluster_index);

  void HighlightPatch(Patch patch);

  std::vector<VolumeParams> GetVolumeParams() {
    return params_per_volume_;
  }

private:
  bool PatchIsWithinQuery(QueryData& query, Patch& patch);
  void WritePatchSetDataToDisk(std::shared_ptr<PatchSet> patch_set, std::string dir);
  void WritePatchInfoToFile(std::ofstream& file, Patch& patch);
  void HackDeleteTextFilesInDir(std::string dir);
  void LoadVolumeParams(std::string filename);
  void WriteVolumeParamsToFile(std::ofstream& file, Patch& patch);
  bool IsVolumeIdInVector(std::vector<int>& vec, int id);

  std::string clusters_dir_;
  std::string root_clusters_dir_;
  
  int cur_patch_set_index_;
  std::vector<std::shared_ptr<PatchSet>> patch_sets_;

  std::shared_ptr<ParallelCoordsExplorer> global_parcoords_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> local_parcoords_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> global_params_explorer_;
  std::shared_ptr<ParallelCoordsExplorer> local_params_explorer_;

  std::shared_ptr<PatchExplorer> patch_explorer_;
  std::vector<VolumeParams> params_per_volume_;
};

#endif  // VOLUMEPATCHANALYSIS_APPLICATIONMANAGER_H_
