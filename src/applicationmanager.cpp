#include <fstream>
#include <iostream>
#include <sstream>
#include <memory>

//#include <windows.h>

#include "applicationmanager.h"
#include "explorer/parallelcoordsexplorer.h"

ApplicationManager::ApplicationManager(std::string clusters_dir) :
cur_patch_set_index_(0) {

  root_clusters_dir_ = clusters_dir;
  clusters_dir_ = clusters_dir;
  patch_sets_.push_back(std::make_shared<PatchSet>());
}

ApplicationManager::~ApplicationManager() {}

void ApplicationManager::LoadClusterDataFromFile(std::string filename) {

  std::stringstream sss;
  sss << filename << "clusterParams_.txt";
  LoadVolumeParams(sss.str());

  int cluster_index = 0;
  while (true) {

    // Try to open the file for this cluster:
    std::stringstream ss;
    ss << filename << "clusterPerformance_" << cluster_index << ".txt";
    std::ifstream f(ss.str().c_str());
    if (!f.is_open()) {
      printf("Cannot open %s. Stopping.", ss.str().c_str());
      return;
    }

    // If file is open, create a new empty cluster and fill it with patch entries:
    Cluster new_cluster;

    std::string curLine;
    int line_num = 0;
    while (getline(f, curLine)) {

      // skip first line
      if (line_num == 0) {
        line_num++;
        continue;
      }

      // For each line, create a new patch entry:
      Patch new_patch;

      std::istringstream iss(curLine);
      std::string token;
      int token_index = 0;
      while (std::getline(iss, token, ',')) {
        switch (token_index) {
          case 0:
            new_patch.vol_id = std::stoi(token);
            break;
          case 1:
            new_patch.center_info.x = std::stoi(token);
            new_patch.coord.x = std::stoi(token);
            break;
          case 2:
            new_patch.center_info.y = std::stoi(token);
            new_patch.coord.y = std::stoi(token);
            break;
          case 3:
            new_patch.center_info.z = std::stoi(token);
            new_patch.coord.z = std::stoi(token);
            break;
          case 4:
            new_patch.size.x = new_patch.size.y = new_patch.size.z = std::stoi(token);
            break;
          case 5:
            new_patch.center_info.distanceField = std::stof(token);
            break;
          case 6:
            new_patch.center_info.complimentaryPaths = std::stoi(token);
            break;
          case 7:
            new_patch.center_info.interfacePerPatch = std::stoi(token);
            break;
          case 8:
            new_patch.center_info.excitonDiffusionProb = std::stof(token);
            break;
          case 9:
            new_patch.center_info.photonAbsorptionProb = std::stof(token);
            break;
          case 10:
            new_patch.center_info.tortuosityValue = std::stof(token);
            break;
          case 11:
            new_patch.center_info.bottleNeck = std::stof(token);
            break;
        }

        token_index++;
      }

      new_cluster.AddPatch(new_patch);
    }

    f.close();

    new_cluster.orig_cluster_id = cluster_index;
    GetActivePatchSet()->AddCluster(new_cluster);
    printf("Added %d patches to cluster %d.\n", new_cluster.num_patches(), GetActivePatchSet()->num_clusters() - 1);
    cluster_index++;
  }
}

void ApplicationManager::ExploreCluster(int cluster_index, bool force_update) {
  if (local_parcoords_explorer_ == nullptr) {
    return;
  } else {

    if (local_parcoords_explorer_->cur_cluster_index() != cluster_index || force_update) {
      std::string new_source = std::string("file:///") + clusters_dir_ + std::string("clusterPerformance_")
        + std::to_string(cluster_index) + std::string(".txt");
      local_parcoords_explorer_->SetDataSource(new_source, cluster_index);
    }
  }

  if (local_params_explorer_ == nullptr) {
    return;
  } else {
    if (local_params_explorer_->cur_cluster_index() != cluster_index || force_update) {

      GenerateClusterParamsFile(clusters_dir_, cluster_index);
      std::string new_source = std::string("file:///") + clusters_dir_ + std::string("clusterParams_")
        + std::to_string(cluster_index) + std::string(".txt");
      local_params_explorer_->SetDataSource(new_source, cluster_index);
    }
  }
}

void ApplicationManager::ApplyQuery(QueryData query) {

  // Create a new empty patch set and fill it out with filtered patches from current input
  std::shared_ptr<PatchSet> out_patch_set = std::make_shared<PatchSet>();

  const std::shared_ptr<PatchSet> in_cur_patch_set = GetActivePatchSet();

  for (int in_cluster_index = 0; in_cluster_index < in_cur_patch_set->num_clusters(); ++in_cluster_index) {

    Cluster in_cur_cluster = in_cur_patch_set->cluster(in_cluster_index);
    Cluster out_cur_cluster;

    for (int in_patch_index = 0; in_patch_index < in_cur_cluster.num_patches(); ++in_patch_index) {

      Patch in_cur_patch = in_cur_cluster.patch(in_patch_index);

      if (PatchIsWithinQuery(query, in_cur_patch)) {
        out_cur_cluster.AddPatch(in_cur_patch);
      }
    }

    if (out_cur_cluster.num_patches() > 0) {
      out_cur_cluster.orig_cluster_id = in_cur_cluster.orig_cluster_id; // get info about orig cluster id
      out_patch_set->AddCluster(out_cur_cluster);
    }
  }

  // Set the new patch set as active patch set for exploration
  patch_sets_.push_back(out_patch_set);
  cur_patch_set_index_++;

  clusters_dir_ = root_clusters_dir_ + std::string("_") + std::to_string(cur_patch_set_index_) + std::string("/");

  // Create folder
  HackDeleteTextFilesInDir(clusters_dir_);
  RemoveDirectory(clusters_dir_.c_str());
  CreateDirectory(clusters_dir_.c_str(), NULL);

  WritePatchSetDataToDisk(out_patch_set, clusters_dir_);

  if (global_parcoords_explorer_ != nullptr) {
    std::string out_file = std::string("file:///") + clusters_dir_ + std::string("clusterPerformance_.txt");
    global_parcoords_explorer_->SetDataSource(out_file);
  }

  if (global_params_explorer_ != nullptr) {
    std::string out_file = std::string("file:///") + clusters_dir_ + std::string("clusterParams_.txt");
    global_params_explorer_->SetDataSource(out_file);
  }

  ExploreCluster(0, true);
  patch_explorer_->SetPatchSet(GetActivePatchSet(), patch_explorer_->slices_dir());
}

void ApplicationManager::ApplyVolumeColors(QVariantMap volume_colors) {

  if (volume_colors.size() == 0)
    return;

  patch_explorer_->ApplyVolumeColors(volume_colors);
}

void ApplicationManager::ResetData() {

  cur_patch_set_index_ = 0;
  clusters_dir_ = root_clusters_dir_;
  patch_sets_.resize(1);

  if (global_parcoords_explorer_ != nullptr) {
    std::string out_file = std::string("file:///") + clusters_dir_ + std::string("clusterPerformance_.txt");

    global_parcoords_explorer_->SetDataSource(out_file);
  }

  ExploreCluster(0, true);
  patch_explorer_->SetPatchSet(GetActivePatchSet(), patch_explorer_->slices_dir());
}

bool ApplicationManager::PatchIsWithinQuery(QueryData& query, Patch& patch) {

  if (patch.center_info.tortuosityValue >= query.minrange_tortuosity &&
      patch.center_info.tortuosityValue <= query.maxrange_tortuosity &&
      patch.center_info.photonAbsorptionProb >= query.minrange_photon_absorption &&
      patch.center_info.photonAbsorptionProb <= query.maxrange_photon_absorption &&
      patch.center_info.excitonDiffusionProb >= query.minrange_exciton_diff &&
      patch.center_info.excitonDiffusionProb <= query.maxrange_exciton_diff &&
      patch.center_info.distanceField >= query.minrange_distance_field &&
      patch.center_info.distanceField <= query.maxrange_distance_field &&
      patch.center_info.bottleNeck >= query.minrange_bottleneck &&
      patch.center_info.bottleNeck <= query.maxrange_bottleneck &&
      patch.center_info.complimentaryPaths >= query.minrange_comp_paths &&
      patch.center_info.complimentaryPaths <= query.maxrange_comp_paths &&
      patch.center_info.interfacePerPatch >= query.minrange_interface_per_patch &&
      patch.center_info.interfacePerPatch <= query.maxrange_interface_per_patch) {
    return true;
  }
  return false;
}

void ApplicationManager::WritePatchSetDataToDisk(std::shared_ptr<PatchSet> patch_set, std::string dir) {

  if (patch_set == nullptr)
    return;

  std::string out_file_all_filename = dir + std::string("clusterPerformance_.txt");
  std::ofstream out_file_all;
  out_file_all.open(out_file_all_filename.c_str());
  // IMPORTANT: Make sure there are no white spaces because these will be interpreted by parcoords
  // as part of the dimension name.
  out_file_all << "Volume,I,J,K,Patch_Size,Dist,Comp,Interface,Diff,Abs,Tort,BN\n";

  std::string out_file_all_params_filename = dir + std::string("clusterParams_.txt");
  std::ofstream out_file_all_params;
  out_file_all_params.open(out_file_all_params_filename.c_str());
  out_file_all_params << "Volume,length,width,height,d,k1,k2,MC\n";
  std::vector<int> volume_ids_found;
  

  for (int in_cluster_index = 0; in_cluster_index < patch_set->num_clusters(); ++in_cluster_index) {

    Cluster in_cur_cluster = patch_set->cluster(in_cluster_index);

    std::string out_file_filename = dir + std::string("clusterPerformance_") + std::to_string(in_cluster_index) + std::string(".txt");
    std::ofstream out_file;
    out_file.open(out_file_filename.c_str());
    out_file << "Volume,I,J,K,Patch_Size,Dist,Comp,Interface,Diff,Abs,Tort,BN\n";

    for (int in_patch_index = 0; in_patch_index < in_cur_cluster.num_patches(); ++in_patch_index) {

      Patch in_cur_patch = in_cur_cluster.patch(in_patch_index);
      WritePatchInfoToFile(out_file, in_cur_patch);
      WritePatchInfoToFile(out_file_all, in_cur_patch);

      if (!IsVolumeIdInVector(volume_ids_found, in_cur_patch.vol_id)) {

        WriteVolumeParamsToFile(out_file_all_params, in_cur_patch);
        volume_ids_found.push_back(in_cur_patch.vol_id);
      }
    }

    out_file.close();
  }

  out_file_all.close();
  out_file_all_params.close();
}

bool ApplicationManager::IsVolumeIdInVector(std::vector<int>& vec, int id) {
  std::vector<int>::iterator it;
  it = std::find(vec.begin(), vec.end(), id);
  return (it != vec.end());
}

void ApplicationManager::WritePatchInfoToFile(std::ofstream& file, Patch& patch) {

  file << patch.vol_id << ",";
  file << patch.center_info.x << ",";
  file << patch.center_info.y << ",";
  file << patch.center_info.z << ",";
  file << patch.size.x << ",";
  file << patch.center_info.distanceField << ",";
  file << patch.center_info.complimentaryPaths << ",";
  file << patch.center_info.interfacePerPatch << ",";
  file << patch.center_info.excitonDiffusionProb << ",";
  file << patch.center_info.photonAbsorptionProb << ",";
  file << patch.center_info.tortuosityValue << ",";
  file << patch.center_info.bottleNeck << "\n";
}

void ApplicationManager::HackDeleteTextFilesInDir(std::string dir) {
  std::string command = "del /Q ";
  std::string win_dir = dir;
  while (true) {
    std::size_t found = win_dir.find("/");
    if (found != std::string::npos) {
      win_dir.replace(win_dir.find("/"), 1, "\\");
    } else {
      break;
    }
  }

  std::string path = win_dir + std::string("*.txt");
  system(command.append(path).c_str());
}

void ApplicationManager::LoadVolumeParams(std::string filename) {

  std::ifstream f(filename.c_str());
  if (!f.is_open()) {
    printf("Cannot open %s. Stopping.", filename.c_str());
    return;
  }

  std::string curLine;
  int line_num = 0;
  while (getline(f, curLine)) {

    // skip first line
    if (line_num == 0) {
      line_num++;
      continue;
    }

    // For each line, create a new volume params entry:
    VolumeParams volume_params;

    std::istringstream iss(curLine);
    std::string token;
    int token_index = 0;
    while (std::getline(iss, token, ',')) {
      switch (token_index) {
        case 0:
          volume_params.vol_id = std::stoi(token);
          break;
        case 1:
          volume_params.length = std::stoi(token);
          break;
        case 2:
          volume_params.width = std::stoi(token);
          break;
        case 3:
          volume_params.height = std::stoi(token);
          break;
        case 4:
          volume_params.d = std::stof(token);
          break;
        case 5:
          volume_params.k1 = std::stof(token);
          break;
        case 6:
          volume_params.k2 = std::stof(token);
          break;
        case 7:
          volume_params.MC = std::stoi(token);
          break;
      }

      token_index++;
    }

    params_per_volume_.push_back(volume_params);
  }

  f.close();
}

VolumeParams ApplicationManager::GetVolumeParams(int volume_id) {
  for (size_t i = 0; i < params_per_volume_.size(); ++i) {
    if (params_per_volume_[i].vol_id == volume_id)
      return params_per_volume_[i];
  }
  printf("ERROR: Trying to fetch non existing volume %d.\n", volume_id);
}

void ApplicationManager::GenerateClusterParamsFile(std::string clusters_dir, int cluster_index) {
  
  std::string out_filename = clusters_dir + std::string("clusterParams_") + std::to_string(cluster_index) + std::string(".txt");
  std::ofstream out_file;
  out_file.open(out_filename.c_str());
  // IMPORTANT: Make sure there are no white spaces because these will be interpreted by parcoords
  // as part of the dimension name.
  out_file << "Volume,length,width,height,d,k1,k2,MC\n";

  Cluster cur_cluster = GetActivePatchSet()->cluster(cluster_index);

  // Here we assume the patches are enumerated such that patches coming from one volume are all together
  int cur_vol_id = -1;  // 
  for (int in_patch_index = 0; in_patch_index < cur_cluster.num_patches(); ++in_patch_index) {

    Patch in_cur_patch = cur_cluster.patch(in_patch_index);
    if (cur_vol_id != in_cur_patch.vol_id) {
      WriteVolumeParamsToFile(out_file, in_cur_patch);
      cur_vol_id = in_cur_patch.vol_id;
    }
  }

  out_file.close();
}

void ApplicationManager::HighlightPatch(Patch patch) {
  if (global_parcoords_explorer_ == nullptr)
    return;

  global_parcoords_explorer_->HighlightPatch(patch);
}

void ApplicationManager::WriteVolumeParamsToFile(std::ofstream& file, Patch& patch) {

  const VolumeParams vol_params = GetVolumeParams(patch.vol_id);
  file << vol_params.vol_id << ",";
  file << vol_params.length << ",";
  file << vol_params.width << ",";
  file << vol_params.height << ",";
  file << vol_params.d << ",";
  file << vol_params.k1 << ",";
  file << vol_params.k2 << ",";
  file << vol_params.MC << "\n";
}