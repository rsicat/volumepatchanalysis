#include "parallelcoordsexplorer.h"
#include "applicationmanager.h"

#include <QVBoxLayout>
#include <QWebFrame>

ParallelCoordsExplorer::ParallelCoordsExplorer(std::string html_filename, std::string data_source, QWidget *parent) : QWebView(parent), application_manager_(nullptr), web_inspector_(nullptr){
  
  html_filename_ = html_filename;
  SetDataSource(data_source);

#ifdef _DEBUG 
  web_inspector_ = new QWebInspector();
  web_inspector_->setPage(this->page());

  web_inspector_dialog_ = new QDialog();
  web_inspector_dialog_->setLayout(new QVBoxLayout());
  web_inspector_dialog_->layout()->addWidget(web_inspector_);
  web_inspector_dialog_->setModal(false);
  web_inspector_dialog_->open();
  web_inspector_dialog_->show();
  web_inspector_dialog_->raise();
  web_inspector_dialog_->activateWindow();

  web_inspector_->setDisabled(false);
#endif 
}

ParallelCoordsExplorer::~ParallelCoordsExplorer() {

}

void ParallelCoordsExplorer::SetDataSource(std::string source_file, int cluster_index) {
  
  cur_cluster_index_ = cluster_index;
  web_data_.ClearAll();
  web_data_.SetParent(this);  QVariantMap &var_data = web_data_.getData();
  QString str = QString(source_file.c_str());
  var_data.insert("data_source", str);
  
  SetUpQueryData();
  SetUpVolumeColors();
  
  this->load(QUrl::fromLocalFile(html_filename_.c_str()));
  
  AttachObject();
  connect(this->page()->mainFrame(), &QWebFrame::javaScriptWindowObjectCleared, this, &ParallelCoordsExplorer::AttachObject);

  //this->triggerPageAction(QWebPage::ReloadAndBypassCache);
  //this->reload();

#ifdef _DEBUG
  if (web_inspector_ != nullptr) {
    web_inspector_->setPage(this->page());
  }
#endif
}

void ParallelCoordsExplorer::SetPatchSet(std::shared_ptr<PatchSet> patch_set) {

  //this->->page()->mainFrame()->addToJavaScriptWindowObject("myoperations", &data);
}

void ParallelCoordsExplorer::SetUpQueryData() {

  QVariantMap &web_query = web_data_.getQuery();
  web_query.insert("minrange_tortuosity", default_query_data_.minrange_tortuosity);
  web_query.insert("maxrange_tortuosity", default_query_data_.maxrange_tortuosity);
  web_query.insert("minrange_photon_absorption", default_query_data_.minrange_photon_absorption);
  web_query.insert("maxrange_photon_absorption", default_query_data_.maxrange_photon_absorption);
  web_query.insert("minrange_exciton_diff", default_query_data_.minrange_exciton_diff);
  web_query.insert("maxrange_exciton_diff", default_query_data_.maxrange_exciton_diff);
  web_query.insert("minrange_distance_field", default_query_data_.minrange_distance_field);
  web_query.insert("maxrange_distance_field", default_query_data_.maxrange_distance_field);
  web_query.insert("minrange_comp_paths", default_query_data_.minrange_comp_paths);
  web_query.insert("maxrange_comp_paths", default_query_data_.maxrange_comp_paths);
  web_query.insert("minrange_interface_per_patch", default_query_data_.minrange_interface_per_patch);
  web_query.insert("maxrange_interface_per_patch", default_query_data_.maxrange_interface_per_patch);
  web_query.insert("minrange_bottleneck", default_query_data_.minrange_bottleneck);
  web_query.insert("maxrange_bottleneck", default_query_data_.maxrange_bottleneck);
}

void ParallelCoordsExplorer::SetUpVolumeColors() {
  
  // Fetch existing volume ids from data
  if (application_manager_ == nullptr)
    return;

  QVariantMap &volume_colors = web_data_.getVolumeColors();
  std::vector<VolumeParams> volume_params = application_manager_->GetVolumeParams();

  for (int v = 0; v < volume_params.size(); ++v) {
    volume_colors.insert(QString(std::to_string(volume_params[v].vol_id).c_str()), QString("empty"));
  }
}

void ParallelCoordsExplorer::SendQueryToAppManager() {

  // Get query from web
  QVariantMap &web_query = web_data_.getQuery();

  // Assemble query
  query_data_.minrange_tortuosity = web_query["minrange_tortuosity"].toFloat();
  query_data_.maxrange_tortuosity = web_query["maxrange_tortuosity"].toFloat();
  query_data_.minrange_photon_absorption = web_query["minrange_photon_absorption"].toFloat();
  query_data_.maxrange_photon_absorption = web_query["maxrange_photon_absorption"].toFloat();
  query_data_.minrange_exciton_diff = web_query["minrange_exciton_diff"].toFloat();
  query_data_.maxrange_exciton_diff = web_query["maxrange_exciton_diff"].toFloat();
  query_data_.minrange_distance_field = web_query["minrange_distance_field"].toFloat();
  query_data_.maxrange_distance_field = web_query["maxrange_distance_field"].toFloat();
  query_data_.minrange_comp_paths = web_query["minrange_comp_paths"].toInt();
  query_data_.maxrange_comp_paths = web_query["maxrange_comp_paths"].toInt();
  query_data_.minrange_interface_per_patch = web_query["minrange_interface_per_patch"].toInt();
  query_data_.maxrange_interface_per_patch = web_query["maxrange_interface_per_patch"].toInt();
  query_data_.minrange_bottleneck = web_query["minrange_bottleneck"].toFloat();
  query_data_.maxrange_bottleneck = web_query["maxrange_bottleneck"].toFloat();
  
  // Send query to app manager and apply to patch set
  if (application_manager_ == nullptr)
    return;

  // Clear the javascript object qtobject
  
  application_manager_->ApplyQuery(query_data_);
}

void ParallelCoordsExplorer::SendVolumeColorsToAppManager() {

  // Get colors from web
  QVariantMap &volume_colors = web_data_.getVolumeColors();

  if (application_manager_ == nullptr)
    return;

  application_manager_->ApplyVolumeColors(volume_colors);
}

void ParallelCoordsExplorer::ResetDataInAppManager() {
  if (application_manager_ == nullptr) return;

  application_manager_->ResetData();
}

void ParallelCoordsExplorer::HighlightPatch(Patch patch) {

  // Get data from web
  QVariantMap &web_highlighted_data = web_data_.getHighlightedData();
  web_highlighted_data.clear();

  // Assemble data
  web_highlighted_data.insert("Volume", patch.vol_id);
  web_highlighted_data.insert("I", patch.center_info.x);
  web_highlighted_data.insert("J", patch.center_info.y);
  web_highlighted_data.insert("K", patch.center_info.z);
  web_highlighted_data.insert("Patch_Size", patch.size.x);
  web_highlighted_data.insert("Dist", patch.center_info.distanceField);
  web_highlighted_data.insert("Comp", patch.center_info.complimentaryPaths);
  web_highlighted_data.insert("Interface", patch.center_info.interfacePerPatch);
  web_highlighted_data.insert("Diff", patch.center_info.excitonDiffusionProb);
  web_highlighted_data.insert("Abs", patch.center_info.photonAbsorptionProb);
  web_highlighted_data.insert("Tort", patch.center_info.tortuosityValue);
  web_highlighted_data.insert("BN", patch.center_info.bottleNeck);

  this->page()->mainFrame()->evaluateJavaScript("UpdateHighlight()");
}