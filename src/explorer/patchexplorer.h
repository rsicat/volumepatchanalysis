#ifndef VOLUMEPATCHEXPLORER_EXPLORER_PATCHEXPLORER_H_
#define VOLUMEPATCHEXPLORER_EXPLORER_PATCHEXPLORER_H_

#include <memory>

#include <QCache>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>

#include "explorer/drawtile.h"
#include "explorer/texturecache.h"
#include "data_structures/patchset.h"
#include "data_structures/cluster.h"

struct HighlightedPatch {

  Patch patch;
  int patch_index;
  int orig_cluster_index;
};

class ApplicationManager;
QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram);
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture);

class PatchExplorer : public QOpenGLWidget {
  Q_OBJECT

public:
  explicit PatchExplorer(QTextureCache* texture_cache,QWidget *parent = 0);
  ~PatchExplorer();
  QSize minimumSizeHint() const Q_DECL_OVERRIDE;
  QSize sizeHint() const Q_DECL_OVERRIDE;
  void SetClearColor(const QColor &color);

  void SetPatchSet(std::shared_ptr<PatchSet> patch_set, std::string slices_dir);
  void SetApplicationManager(std::shared_ptr<ApplicationManager> application_manager);
  std::string slices_dir() {
    return slices_dir_;
  }

  void ApplyVolumeColors(QVariantMap volume_colors);

signals:
  void clicked();

protected:
  void initializeGL() Q_DECL_OVERRIDE;
  void CleanupGL();
  void paintGL() Q_DECL_OVERRIDE;
  void paintQt();
  void wheelEvent(QWheelEvent *event);
  void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

  QOpenGLFunctions *QGL = nullptr;

private:
  void DrawPatches();
  // Draw row of patches; return maximum height drawn.
  int DrawPatchRow(Cluster& cluster_to_draw, int start_index, QPoint upper_left_corner, int available_width);
  // Start drawing left to right with the first patch drawn at the given upper_left_corner coords.
  void DrawSinglePatch(DrawTile *drawer, Patch& patch_coords, QSize draw_size,
                       QPoint upper_left_corner);
  // Return the row index that was clicked given mouse position along y
  int GetClickedRowIndex(int mouse_pos_y);
  int GetClickedColIndex(int patch_row, int mouse_pos_x);
  void WritePatchInforToFile(int row, int col, std::string filename);
  void SetPatchToHighLight(int row, int col);
  void ShiftVisiblePatchesInRow(int row, int num_patches_to_shift);

  std::shared_ptr<PatchSet> patch_set_;      // current patch set being explored
  std::shared_ptr<DrawTile> draw_tile_;
  QTextureCache* texture_cache_;
  std::shared_ptr<ApplicationManager> application_manager_;

  float draw_size_scale_;
  int num_pixels_offset_x_start;
  int num_pixels_offset_x;
  int num_pixels_offset_y;
  int num_patches_to_shift_;
  QColor clear_color_;
  QColor patch_border_color_;
  QColor highlighted_patch_border_color_;
  float patch_border_percentage_;
  QPoint last_mouse_pos_;
  QSize max_patch_size_;
  int start_cluster_index_;
  int num_visible_clusters_scroll_increment_;
  std::vector<int> per_cluster_start_index_;
  std::vector<int> height_used_by_row_;
  std::vector<std::vector<int>> width_used_by_patch_per_row_;   // row, patch
  std::string slices_dir_;
  HighlightedPatch highlighted_patch_;
  QVariantMap volume_colors_; // maps string volume_id to QColor string hex
};

#endif  // VOLUMEPATCHEXPLORER_EXPLORER_PATCHEXPLORER_H_
