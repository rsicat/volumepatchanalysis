#ifndef PARALLEL_COORDS_EXPLORER_H_
#define PARALLEL_COORDS_EXPLORER_H_

#include <memory>

#include <QtWebKitWidgets/QWebView>
#include <QWebInspector>
#include <QDialog>

#include "data_structures/patchset.h"
#include "webdata.h"

class ApplicationManager;

class ParallelCoordsExplorer : public QWebView {
  Q_OBJECT

public:
  ParallelCoordsExplorer(std::string html_filename, std::string data_source, QWidget *parent = 0);
  ~ParallelCoordsExplorer();

  void SetPatchSet(std::shared_ptr<PatchSet> patch_set);
  void SetDataSource(std::string source_file, int cluster_index = -1);
  void SetApplicationManager(std::shared_ptr<ApplicationManager> application_manager) {
    application_manager_ = application_manager;
    SetUpVolumeColors();
  }
  void SendQueryToAppManager();
  void SendVolumeColorsToAppManager();
  void ResetDataInAppManager();
  void HighlightPatch(Patch patch);
  int cur_cluster_index() {
    return cur_cluster_index_;
  }

private:
  void AttachObject() {
    this->page()->mainFrame()->addToJavaScriptWindowObject("qtobject", &web_data_, QWebFrame::QtOwnership);
  }
  void SetUpQueryData();
  void SetUpVolumeColors();

  QWebInspector* web_inspector_;
  QDialog* web_inspector_dialog_;
  WebData web_data_;
  QVariant source_name;
  QueryData query_data_;
  QueryData default_query_data_;
  std::string html_filename_;
  int cur_cluster_index_;

  std::shared_ptr<ApplicationManager> application_manager_;
};

#endif // PARALLEL_COORDS_EXPLORER_H_
