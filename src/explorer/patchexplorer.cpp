#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QPainter>

#include <iostream>
#include <fstream>

#include "patchexplorer.h"
#include "applicationmanager.h"

//////////////////////////////////////////////////////////////////////////
// PUBLIC MEMBERS:
//////////////////////////////////////////////////////////////////////////

PatchExplorer::PatchExplorer(QTextureCache* texture_cache, QWidget *parent)
: QOpenGLWidget(parent),
patch_set_(nullptr),
draw_tile_(std::make_shared<DrawTile>(this)),
clear_color_(205, 212, 235),
//clear_color_(217, 232, 223),
//patch_border_color_(Qt::black),
patch_border_color_(205, 212, 235, 255),
//patch_border_color_(Qt::red),
highlighted_patch_border_color_(Qt::magenta),
patch_border_percentage_(0.025f),
last_mouse_pos_(QPoint(0, 0)),
start_cluster_index_(0),
num_visible_clusters_scroll_increment_(4),// number of clusters per scroll
application_manager_(nullptr),
num_pixels_offset_x_start(40),
num_pixels_offset_x(4),
num_pixels_offset_y(16),
draw_size_scale_(2.0f),
num_patches_to_shift_(8) {   // spacing between cluster row

  volume_colors_.clear();
  texture_cache_ = texture_cache;
}

PatchExplorer::~PatchExplorer() {

}

QSize PatchExplorer::minimumSizeHint() const {
  return QSize(200, 200);
}

QSize PatchExplorer::sizeHint() const {
  return QSize(64 * 15, 64 * 6);
}

void PatchExplorer::SetClearColor(const QColor &color) {
  clear_color_ = color;
  update();
}

void PatchExplorer::SetPatchSet(std::shared_ptr<PatchSet> patch_set, std::string slices_dir) {
  if (patch_set == nullptr) return;

  slices_dir_ = slices_dir;
  patch_set_ = patch_set;
  start_cluster_index_ = 0;
  max_patch_size_ = QSize(patch_set_->max_patch_size().x, patch_set_->max_patch_size().z);
  per_cluster_start_index_.resize(patch_set_->num_clusters(), 0);
  
  update();
}

void PatchExplorer::SetApplicationManager(std::shared_ptr<ApplicationManager> application_manager) {
  if (application_manager == nullptr)
    return;

  application_manager_ = application_manager;
}

void PatchExplorer::ApplyVolumeColors(QVariantMap volume_colors) {
  if (volume_colors.size() == 0)
    return;

  volume_colors_ = volume_colors;

  for (QVariantMap::const_iterator it = volume_colors_.begin(); it != volume_colors_.end(); ++it) {
    qDebug() << it.key() << ", " << it.value() << "\n";
  }

  update();
}


//////////////////////////////////////////////////////////////////////////
// PROTECTED MEMBERS:
//////////////////////////////////////////////////////////////////////////

void PatchExplorer::initializeGL() {
  if (width() == 0 || height() == 0) {
    printf("Cannot initialize without PatchExplorer visible.\n");
    return;
  }
  if (context() == nullptr) {
    return;
  }
  connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &PatchExplorer::CleanupGL);
  if (QGL == nullptr) {
    QGL = new QOpenGLFunctions();
    QGL->initializeOpenGLFunctions();
  }

  if (patch_set_ == nullptr)
    return;

  draw_tile_->Init(QSize(20,20));
  draw_tile_->UpdateBorderPercentage(patch_border_percentage_);
  draw_tile_->UpdateBorderColor(patch_border_color_);
}

void PatchExplorer::CleanupGL() {
  if (QGL != nullptr && context()->isValid()) {
    makeCurrent();
    delete(QGL);
    QGL = nullptr;
    doneCurrent();
  }
}


void PatchExplorer::paintGL() {
  QPainter painter(this);
  painter.beginNativePainting();

  QGL->glClearColor(clear_color_.redF(), clear_color_.greenF(), clear_color_.blueF(),
                    clear_color_.alphaF());
  QGL->glClear(GL_COLOR_BUFFER_BIT);

  if (patch_set_ == nullptr)
    return;

  DrawPatches();
  
  painter.endNativePainting();

  paintQt();
}

void PatchExplorer::paintQt() {
  QPainter painter(this);

  QColor text_color = QColor(0, 0, 0);
  const int fontSize = 10;
  painter.setPen(text_color);
  painter.setFont(QFont("helvetica", fontSize));

  int cur_height = 15;
  for (int r = 0; r < height_used_by_row_.size(); ++r) {

    // write the id or index of the original cluster 
    Cluster cur_cluster = patch_set_->cluster(start_cluster_index_ + r);
    painter.drawText(5, cur_height,
                     QString::number(cur_cluster.orig_cluster_id));

    //painter.drawLine(0, cur_height, width(), cur_height);

    cur_height += height_used_by_row_[r];
  }
}

void PatchExplorer::wheelEvent(QWheelEvent *event) {
  if (patch_set_ == nullptr)
    return;

  if (event->modifiers() & Qt::ShiftModifier) {
    
    int current_row = GetClickedRowIndex(event->pos().y());

    if (event->delta() < 0) {
      ShiftVisiblePatchesInRow(current_row, num_patches_to_shift_);
    } else {
      ShiftVisiblePatchesInRow(current_row, -num_patches_to_shift_);
    }
  } else {

    if (event->delta() < 0) {
      start_cluster_index_ += num_visible_clusters_scroll_increment_;
      start_cluster_index_ = std::min(start_cluster_index_, patch_set_->num_clusters());
     
    } else {
      start_cluster_index_ -= num_visible_clusters_scroll_increment_;
      start_cluster_index_ = std::max(0, start_cluster_index_);
    }

    update();
  }

  event->accept();
}

void PatchExplorer::mousePressEvent(QMouseEvent *event) {
  if (patch_set_ == nullptr)
    return;

  int dx = event->x() - last_mouse_pos_.x();
  int current_row = GetClickedRowIndex(event->pos().y());
  int current_col = GetClickedColIndex(current_row, event->pos().x());

  // Show selected bin's patches on the local par coords interface
  if (event->button() == Qt::RightButton) {
    
    if (application_manager_ == nullptr)
      return;

    int current_cluster_index = current_row + start_cluster_index_;
    application_manager_->ExploreCluster(current_cluster_index);

    WritePatchInforToFile(current_row, current_col, std::string("SelectedPatch.txt"));
    SetPatchToHighLight(current_row, current_col);
    update();
  }

  last_mouse_pos_ = event->pos();
}

//////////////////////////////////////////////////////////////////////////
// PRIVATE MEMBERS:
//////////////////////////////////////////////////////////////////////////

void PatchExplorer::DrawPatches() {
  
  int height_used_so_far = 0;
  int cur_cluster_index = start_cluster_index_;
  height_used_by_row_.clear();
  width_used_by_patch_per_row_.clear();

  // Draw rows while the row fits in the window height:
  while (height_used_so_far < height() && cur_cluster_index < patch_set_->num_clusters()) {

    Cluster cur_cluster = patch_set_->cluster(cur_cluster_index);
    int height_left = height() - height_used_so_far;
    if (cur_cluster.max_patch_size().z > height_left) return;
    
    QPoint uppper_left_corner(num_pixels_offset_x_start, height_used_so_far);
    std::vector<int> empty_vec;
    width_used_by_patch_per_row_.push_back(empty_vec);
    int height_used_by_row = DrawPatchRow(cur_cluster, per_cluster_start_index_[cur_cluster_index], uppper_left_corner,
                                          width()) + num_pixels_offset_y;

    height_used_by_row_.push_back(height_used_by_row);

    cur_cluster_index++;
    height_used_so_far += height_used_by_row;
  }
}

int PatchExplorer::DrawPatchRow(Cluster& cluster_to_draw, int start_index,
                                 QPoint upper_left_corner, int available_width) {

  QPoint local_translation = upper_left_corner;
  const int num_patches_available = cluster_to_draw.num_patches();
  int cur_index = start_index;
  int width_remaining = available_width;
  int max_height_drawn = 0;
  start_index = std::min(start_index, num_patches_available - 1); // Make sure we have a valid starting index

  const bool use_volume_colored_boundaries = volume_colors_.size() > 0;

  while (cur_index < cluster_to_draw.num_patches() && width_remaining > 0) {

    Patch cur_patch = cluster_to_draw.patch(cur_index);
    QSize draw_size(cur_patch.size.x * draw_size_scale_, cur_patch.size.z * draw_size_scale_);  // TODO: Make parameter
    width_remaining -= draw_size.width() + num_pixels_offset_x;
    if (width_remaining <= 0)     // Don't draw patch if it won't fit in width anymore
      return max_height_drawn;

    max_height_drawn = std::max(max_height_drawn, draw_size.height());

    bool highlight_this_patch = (highlighted_patch_.patch.vol_id != -1 && 
                                 highlighted_patch_.orig_cluster_index == cluster_to_draw.orig_cluster_id &&
                                 highlighted_patch_.patch_index == cur_index);

    // For highlighting, temporarily adjust the border settings
    if (highlight_this_patch) {
      draw_tile_->UpdateBorderPercentage(patch_border_percentage_ * 4);
      draw_tile_->UpdateBorderColor(highlighted_patch_border_color_);
    } else if (use_volume_colored_boundaries) {

      QVariantMap::const_iterator it = volume_colors_.find(QString(std::to_string(cur_patch.vol_id).c_str()));
      if (it != volume_colors_.end()) {
        draw_tile_->UpdateBorderPercentage(patch_border_percentage_ * 2);
        QString a = volume_colors_[QString(cur_patch.vol_id)].toString();
        qDebug() << a << "\n";
        QColor r = QColor(it.value().toString());
        draw_tile_->UpdateBorderColor(r);
      }
    }

    DrawSinglePatch(draw_tile_.get(), cur_patch, draw_size, local_translation);

    // After highlighting, adjust back the original border settings
    if (highlight_this_patch || use_volume_colored_boundaries) {
      draw_tile_->UpdateBorderPercentage(patch_border_percentage_);
      draw_tile_->UpdateBorderColor(patch_border_color_);
    }

    local_translation.setX(local_translation.x() + draw_size.width() + num_pixels_offset_x);

    width_used_by_patch_per_row_[width_used_by_patch_per_row_.size() - 1].push_back(draw_size.width() + num_pixels_offset_x);

    cur_index++;
  }
  
  return max_height_drawn;
}

void PatchExplorer::DrawSinglePatch(DrawTile *drawer, Patch& patch, QSize draw_size,
                                    QPoint upper_left_corner) {

  drawer->SetDrawSize(draw_size);

  QOpenGLTexture* texture_to_use = texture_cache_->GetTexture(GetPatchSliceFileName(slices_dir_, patch.vol_id, patch.coord.y));

  if (texture_to_use == nullptr)
    return;

  QSize texture_image_size(texture_to_use->width(), texture_to_use->height());

  // Modify the texture coordinates so they are only covering the draw area.
  // Draw a quad in place of the patch where it's supposed to be drawn and apply the texture.   
  QPointF texcoords_shift(
    (float(int(patch.coord.x) - (draw_size.width()/draw_size_scale_) / 2) / float(texture_image_size.width())),
    (float(int(patch.coord.z) - (draw_size.height()/draw_size_scale_) / 2) / float(texture_image_size.height()))
    );

  drawer->UpdateTextureCoordsShift(texcoords_shift);

  QPointF draw_size_to_img_size_ratio(
  (float(draw_size.width())/draw_size_scale_) / float(texture_image_size.width()),
    (float(draw_size.height())/draw_size_scale_) / float(texture_image_size.height()));
  drawer->UpdateTextureCoordsScale(draw_size_to_img_size_ratio);

  drawer->DrawTileAt(upper_left_corner, texture_to_use);
}

int PatchExplorer::GetClickedRowIndex(int mouse_pos_y) {
  if (height_used_by_row_.size() == 0)
    return 0;

  int height_used = 0;
  for (int r = 0; r < height_used_by_row_.size(); ++r) {
    height_used += height_used_by_row_[r];

    if (mouse_pos_y <= height_used) {
      return r;
    }
  }

  return 0;
}

int PatchExplorer::GetClickedColIndex(int patch_row, int mouse_pos_x) {
  if (width_used_by_patch_per_row_.size() == 0)
    return 0;

  int width_used = num_pixels_offset_x_start;
  for (int c = 0; c < width_used_by_patch_per_row_[patch_row].size(); ++c) {
    width_used += width_used_by_patch_per_row_[patch_row][c];

    if (mouse_pos_x <= width_used) {
      return c;
    }
  }

  return 0;
}

void PatchExplorer::WritePatchInforToFile(int row, int col, std::string filename) {
  if (patch_set_ == nullptr)
    return;

  int cluster_index = row + start_cluster_index_;
  const Patch cur_patch = patch_set_->cluster(cluster_index).patch(col);

  std::ofstream out_file;
  out_file.open(filename.c_str());
  out_file << patch_set_->cluster(cluster_index).orig_cluster_id << "\t";
  out_file << cur_patch.vol_id << "\t";
  out_file << cur_patch.center_info.x << "\t";
  out_file << cur_patch.center_info.y << "\t";
  out_file << cur_patch.center_info.z << "\t";
  out_file << cur_patch.half_size.x << "\t";
  out_file << cur_patch.half_size.y << "\t";
  out_file << cur_patch.half_size.z << "\t";
  out_file.close();
}

void PatchExplorer::SetPatchToHighLight(int row, int col) {

  if (patch_set_ == nullptr)
    return;

  int cluster_index = row + start_cluster_index_;
  int patch_index = col + per_cluster_start_index_[cluster_index];
  highlighted_patch_.patch = patch_set_->cluster(cluster_index).patch(patch_index);
  highlighted_patch_.orig_cluster_index = patch_set_->cluster(cluster_index).orig_cluster_id;
  highlighted_patch_.patch_index = patch_index;

  if (application_manager_ == nullptr)
    return;

  application_manager_->HighlightPatch(highlighted_patch_.patch);
}

void PatchExplorer::ShiftVisiblePatchesInRow(int row, int num_patches_to_shift) {

  int cluster_index = row + start_cluster_index_;
  if (per_cluster_start_index_.size() == 0 || cluster_index >= per_cluster_start_index_.size() ||
      patch_set_ == nullptr)
    return;

  int shifted_start = per_cluster_start_index_[cluster_index] + num_patches_to_shift;
  per_cluster_start_index_[cluster_index] = std::min(std::max(0, shifted_start), patch_set_->cluster(cluster_index).num_patches() - 1);
  update();
}