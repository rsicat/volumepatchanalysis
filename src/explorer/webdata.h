#ifndef VOLUMEPATCHEXPLORER_WEBDATA_H_
#define VOLUMEPATCHEXPLORER_WEBDATA_H_

#include <QCoreApplication>
#include <QDebug>
#include <QApplication>
#include <QtWebKitWidgets/QWebFrame>
#include <QtWebKitWidgets/QWebPage>
#include <QtWebKitWidgets/QWebView>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <QWebSettings>
#include <QVariant>

class ParallelCoordsExplorer;

class WebData : public QObject {
  Q_OBJECT
  Q_PROPERTY(QVariantMap data READ getData WRITE setData)
  Q_PROPERTY(QVariantMap query READ getQuery WRITE setQuery)
  Q_PROPERTY(QVariantMap highlighted_data READ getHighlightedData)
  Q_PROPERTY(QVariantMap volume_colors READ getVolumeColors)

public:

  WebData(ParallelCoordsExplorer *parent = 0);

  ~WebData();

  void ClearAll() {
    data.clear();
    query.clear();
  }

  QVariantMap & getData() {
    return data;
  }

  QVariantMap & getQuery() {
    return query;
  }

  QVariantMap & getHighlightedData() {
    return highlighted_data;
  }

  QVariantMap & getVolumeColors() {
    return volume_colors;
  }

  void SetParent(ParallelCoordsExplorer* parent) {
    parent_ = parent;
  }

  Q_INVOKABLE void applyQuery();
  Q_INVOKABLE void applyVolumeColors();
  Q_INVOKABLE void resetData();
  Q_INVOKABLE void setData(const QVariantMap& data_new) {
    data = data_new;
  }

  Q_INVOKABLE void setQuery(const QVariantMap& q) {
    query = q;
  }

  Q_INVOKABLE void setQueryValInt(const QString& name, int val) {
    query[name].setValue(val);
  }

  Q_INVOKABLE void setQueryValFloat(const QString& name, float val) {
    query[name].setValue(val);
  }

  Q_INVOKABLE void setDataValString(const QString& name, const QString& val) {
    data[name].setValue(val);

    qDebug() << data["color1"].toString();
  }

  Q_INVOKABLE void setVolumeColor(const QString& volume_id, const QString& color) {
    volume_colors[volume_id].setValue(color);
  }

private:

  QVariantMap data;
  QVariantMap volume_colors;
  QVariantMap query;
  QVariantMap highlighted_data;
  ParallelCoordsExplorer* parent_;
};


#endif  // VOLUMEPATCHEXPLORER_EXPLORER_DRAWTILE_H_
