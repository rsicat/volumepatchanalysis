#include <QDebug>

#include "webdata.h"
#include "explorer/parallelcoordsexplorer.h"

WebData::WebData(ParallelCoordsExplorer *parent)
: parent_(parent) {

}

WebData::~WebData() {

}

void WebData::applyQuery() {

  qDebug() << "Apply Query button pressed.\n";
  parent_->SendQueryToAppManager();
}

void WebData::applyVolumeColors() {

  qDebug() << "Apply Volume Colors button pressed.\n";
  parent_->SendVolumeColorsToAppManager();
}

void WebData::resetData() {

  qDebug() << "Reset Data button pressed.\n";
  parent_->ResetDataInAppManager();
}