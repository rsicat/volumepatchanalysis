# README #

## Research Questions: ##

Note: In **bold text** are the visual analysis components of the workflow that I plan to implement EXCEPT for the 3D volume view which would be Amira.

Q1. Is it possible to cluster patches according to shape similarity?

ASSUMPTION: Yes. If no, at least we now know, then we just skip the clustering and do analysis on ALL patches together.

EVALUATION: Visual inspection via 1) **cluster view**, and 2) **3D volume (morphology) view**.

1.a. What patch-based feature (and parameters) should be used?

1.b. What clustering method (and parameters) should be used?


Q2. Are there shape clusters that correspond to good performance?

ASSUMPTION: Yes. If no, at least we now know, then we just skip the clustering and do analysis on ALL patches together.

EVALUATION: To check if a particular shape cluster [this cluster can be ALL patches together] corresponds to good performance, we can compute the *probability (P)* [probability could make sense since we are making general assumptions about the whole space of patch possibilities] that the patches w/in the cluster belong to regions in performance space that are considered good performance. So the user first specifies a "mask/query/subspace" in performance space that corresponds to what he/she considers to be good performance e.g. specify ranges along each performance dimension. Visually, this can be evaluated by looking at a **visualization of the patches in performance space** and see if they cluster densely in the good performance regions that are for example highlighted (if there are no such clustering i.e. the patches are uniformly/randomly spread across performance space, this means the shape cluster does not correspond to good performance). For this view, I recommend parallel coordinates view/interface [[like this](https://syntagmatic.github.io/parallel-coordinates/examples/brushing.html)]. To avoid individual inspection of clusters, we can also use the computed *probabilities (P)* and use them as quick and simple metric i.e. higher *probability (P)* is good.

2.a. Is there a subset of good performance space where a shape cluster corresponds to? Maybe if there is no "perfect" cluster where all the patches cluster densely at the good performance space, we can at least identify sub-spaces (potentially lower dimensional e.g. only good in terms of photon absorption and exciton diffusion) where at least there is dense clustering. This allows us to associate a shape cluster to SOME good performance dimensions/metrics. This sounds more likely to be the case. For more in depth analysis of correlations among parameters and performance, the user can use a **scatterplot matrix visualization/query**.

EVALUATION: We can do this simply by making the good performance mask/query less strict i.e. smaller subspace in performance space and use the same visual tool for evaluating Q2.

Q3. (Once Q2 has been answered and good shape clusters are identified, this is the next question.) For a given shape cluster, what are the parameter ranges that would give me a high probability of generating similar patches within the cluster?

ASSUMPTION: For a given shape cluster, the subspace of the parameters that would generate similar patches can be identified and is small (making it possible to replicate the shapes through the morphology generation).

EVALUATION: First, we use a **visualization of the patches in parameter space** as we used for performance space. This helps us visually determine good parameter ranges (for a particular parameter, a good range is where patches would cluster densely in). Then, we can use the good parameter ranges to generate morphologies and measure the probability of their patches getting clustered with the shape cluster of interest.

INITIAL TEST: Maybe it's a good idea to first test how deterministic/probabilistic the morphology generation is. That is, if we assume we identified a small range of good parameters and use values w/in that range, are we going to get morphologies that are consistent/similar to each other? If the answer here is no, i.e. the generation is TOO RANDOM, then maybe there is something wrong with the generation model?

Q4. (Once Q3 has been answered and good parameter ranges are identified, this is the next question. This is very important to VERIFY THE ASSUMPTION that the global morphology performance relies on its local patch performances.) Does a morphology consisting of good performing shapes perform well globally?

EVALUATION: Using the good parameter ranges identified in Q3, we generate morphologies and measure their global performance. 

_________________________________

## Ideal scenario:

Q1: Using region covariance feature and ?? clustering, we are able to group patches according to shape similarity.

Q2: By using our performance space visualization/query, we are able to quickly identify shape clusters that correspond to good performance.

Q3: By using our paramameter space visualization/query for good performing shape clusters together with our scatter plot matrix visualization/query, we are able to quickly identify parameter ranges (subspace) that would generate these good shapes.

Q4: Using the identified good parameter ranges, we are able to generate morphologies consisting of local patches that have good performance. By measuring global performances, we are able to verify our assumption that morphologies with good performing local patches ALSO has good global performance.


Conclusion: Using our visual analysis application, domain experts are able to gain better insight from their data, giving them the capability to now generate real life morphologies with expected theoretically good performance.

_________________________________

NOTES:

Possible query interface is: 1) parallel coordinates w/ brush for performance space, 2) parallel coordinates w/ brush for parameter space, 3) shape cluster histograms - dynamically showing probability of satisfying performance and parameter space masks: this is better than going through each cluster and inspecting - instead we just specify what we're looking for and SEE RIGHT AWAY which clusters satisfy our specs.